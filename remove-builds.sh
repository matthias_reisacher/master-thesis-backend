#!/usr/bin/env bash
[[ -d dago-core/build ]] && sudo rm -r dago-core/build
[[ -d dago-core/out ]] && sudo rm -r dago-core/out
[[ -d dago-web/build ]] && sudo rm -r dago-web/build
[[ -d dago-web/out ]] && sudo rm -r dago-web/out
