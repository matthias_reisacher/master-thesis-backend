package at.tu.dago.web.mapper;

import at.tu.dago.core.grpc.LifetimeProto.LifetimeResponse;
import at.tu.dago.core.redis.model.Lifetimes;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface LifetimeProtosMapper {

	default LifetimeResponse lifetimeResponse(Lifetimes lifetimes) {
		return LifetimeResponse.newBuilder()
							   .setLifetimes(lifetimeResponseLifetimes(lifetimes))
							   .build();
	}

	default LifetimeResponse.Lifetimes lifetimeResponseLifetimes(Lifetimes lifetimes) {
		return LifetimeResponse.Lifetimes.newBuilder()
										 .addAllIds(lifetimes.getIds())
										 .addAllValues(lifetimes.getValues())
										 .build();
	}

	default LifetimeResponse emptyLifetimeResponse() {
		return LifetimeResponse.newBuilder()
							   .build();
	}
}
