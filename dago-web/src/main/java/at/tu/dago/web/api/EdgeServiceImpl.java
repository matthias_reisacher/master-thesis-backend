package at.tu.dago.web.api;

import at.tu.dago.core.grpc.EdgeServiceGrpc;
import at.tu.dago.core.grpc.LifetimeProto.LifetimeRequest;
import at.tu.dago.core.grpc.LifetimeProto.LifetimeResponse;
import at.tu.dago.core.redis.model.Lifetimes;
import at.tu.dago.core.service.AttributeService;
import at.tu.dago.core.utils.Bencher;
import at.tu.dago.web.api.interceptor.LogInterceptor;
import at.tu.dago.web.mapper.LifetimeProtosMapper;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@GRpcService(interceptors = { LogInterceptor.class })
@Slf4j
public class EdgeServiceImpl extends EdgeServiceGrpc.EdgeServiceImplBase {

	private AttributeService attributeService;
	private LifetimeProtosMapper mapper;
	private Bencher bencher;

	@Autowired
	public EdgeServiceImpl(AttributeService attributeService, LifetimeProtosMapper mapper, Bencher bencher) {
		this.attributeService = attributeService;
		this.mapper = mapper;
		this.bencher = bencher;
	}

	@Override
	public void fetchLifetimes(LifetimeRequest request, StreamObserver<LifetimeResponse> responseObserver) {
		bencher.start();
		log.debug("Fetching edge-lifetimes of page with id = {}", request.getPageId());
		Optional<Lifetimes> lifetimes = attributeService.getEdgeLifetimes(request.getPageId());
		LifetimeResponse response = lifetimes.map(lt -> mapper.lifetimeResponse(lt))
											 .orElse(mapper.emptyLifetimeResponse());
		bencher.measure("Service fetchLifetimes");

		responseObserver.onNext(response);
		responseObserver.onCompleted();
	}
}
