package at.tu.dago.web.api;

import at.tu.dago.core.grpc.LifetimeProto.LifetimeRequest;
import at.tu.dago.core.grpc.NodeServiceGrpc;
import at.tu.dago.core.redis.model.Lifetimes;
import at.tu.dago.core.redis.model.NodeHierarchy;
import at.tu.dago.core.service.AttributeService;
import at.tu.dago.core.service.HierarchyService;
import at.tu.dago.core.utils.Bencher;
import at.tu.dago.web.api.interceptor.LogInterceptor;
import at.tu.dago.web.mapper.LifetimeProtosMapper;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static at.tu.dago.core.grpc.LifetimeProto.LifetimeResponse;
import static at.tu.dago.core.grpc.NodeProtos.*;

@GRpcService(interceptors = { LogInterceptor.class })
@Slf4j
public class NodeServiceImpl extends NodeServiceGrpc.NodeServiceImplBase {

	private HierarchyService hierarchyService;
	private AttributeService attributeService;
	private LifetimeProtosMapper mapper;
	private Bencher bencher;

	@Autowired
	public NodeServiceImpl(HierarchyService hierarchyService, AttributeService attributeService, LifetimeProtosMapper mapper,
			Bencher bencher) {

		this.hierarchyService = hierarchyService;
		this.attributeService = attributeService;
		this.mapper = mapper;
		this.bencher = bencher;
	}

	@Override
	public void fetchHierarchy(HierarchyRequest request, StreamObserver<HierarchyResponse> responseObserver) {
		bencher.start();
		log.debug("Fetching hierarchy of node with id = {}", request.getNodeId());
		NodeHierarchy hierarchy = hierarchyService.getNodeHierarchy(request.getNodeId());

		HierarchyResponse response = HierarchyResponse.newBuilder()
													  .addAllNodeId(hierarchy.getNodeIds())
													  .build();
		bencher.measure("Service fetchHierarchy");

		responseObserver.onNext(response);
		responseObserver.onCompleted();
	}

	@Override
	public void fetchAttributes(AttributesRequest request, StreamObserver<AttributesResponse> responseObserver) {
		bencher.start();
		log.debug("Fetching attributes of node with id = {}", request.getNodeId());
		String attributes = attributeService.getAttributes(request.getNodeId());

		AttributesResponse response = AttributesResponse.newBuilder()
														.setAttributes(attributes)
														.build();
		bencher.measure("Service fetchAttributes");

		responseObserver.onNext(response);
		responseObserver.onCompleted();
	}

	@Override
	public void fetchLifetimes(LifetimeRequest request, StreamObserver<LifetimeResponse> responseObserver) {
		bencher.start();
		log.debug("Fetching node-lifetimes of page with id = {}", request.getPageId());
		Optional<Lifetimes> lifetimes = attributeService.getNodeLifetimes(request.getPageId());
		LifetimeResponse response = lifetimes.map(lt -> mapper.lifetimeResponse(lt))
											 .orElse(mapper.emptyLifetimeResponse());
		bencher.measure("Service fetchLifetimes");

		responseObserver.onNext(response);
		responseObserver.onCompleted();
	}
}
