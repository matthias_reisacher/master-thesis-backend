package at.tu.dago.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({ "at.tu.dago.web", "at.tu.dago.core" })
public class DagoWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(DagoWebApplication.class, args);
	}
}
