package at.tu.dago.web.mapper;

import at.tu.dago.core.redis.model.GraphPage;
import org.mapstruct.Mapper;

import static at.tu.dago.core.grpc.GraphPageProtos.GraphPageResponse;

@Mapper(componentModel = "spring")
public interface GraphPageProtosMapper {

	default GraphPageResponse graphPageResponse(GraphPage graphPage) {
		return GraphPageResponse.newBuilder()
								.setGraphPage(graphPageResponseGraphPage(graphPage))
								.build();
	}

	default GraphPageResponse.GraphPage graphPageResponseGraphPage(GraphPage graphPage) {
		return GraphPageResponse.GraphPage.newBuilder()
										  .setPageId(graphPage.getPageId())
										  .setGraphId(graphPage.getGraphId())
										  .setPageNumber(graphPage.getPageNumber())
										  .setPageDepth(graphPage.getPageDepth())
										  .addAllNodeId(graphPage.getNodeIds())
										  .addAllEdgeId(graphPage.getEdgeIds())
										  .addAllNode(graphPage.getNodes())
										  .addAllEdge(graphPage.getEdges())
										  .build();
	}

	default GraphPageResponse emptyGraphPageResponse() {
		return GraphPageResponse.newBuilder()
								.build();
	}
}
