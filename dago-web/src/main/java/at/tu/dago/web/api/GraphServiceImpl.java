package at.tu.dago.web.api;

import at.tu.dago.core.grpc.EmptyProto;
import at.tu.dago.core.grpc.GraphProtos;
import at.tu.dago.core.grpc.GraphProtos.CreateGraphResponse;
import at.tu.dago.core.grpc.GraphServiceGrpc;
import at.tu.dago.core.psql.model.Graph;
import at.tu.dago.core.service.GraphService;
import at.tu.dago.web.api.interceptor.LogInterceptor;
import at.tu.dago.web.mapper.GraphMapper;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@GRpcService(interceptors = { LogInterceptor.class })
public class GraphServiceImpl extends GraphServiceGrpc.GraphServiceImplBase {

	private GraphService service;
	private GraphMapper mapper;

	@Autowired
	public GraphServiceImpl(GraphService service, GraphMapper mapper) {
		this.service = service;
		this.mapper = mapper;
	}

	@Override
	public void fetchAll(EmptyProto.Empty request, StreamObserver<GraphProtos.GraphResponse> responseObserver) {
		List<Graph> graphs = service.getGraphs();

		responseObserver.onNext(mapper.graphResponse(graphs));
		responseObserver.onCompleted();
	}

	@Override
	public void createGraph(GraphProtos.CreateGraphRequest request, StreamObserver<CreateGraphResponse> responseObserver) {

		long graphId = service.createGraph(request.getName(), request.getDescription());

		CreateGraphResponse response = CreateGraphResponse.newBuilder()
														  .setGraphId(graphId)
														  .build();

		responseObserver.onNext(response);
		responseObserver.onCompleted();
	}
}
