package at.tu.dago.web.mapper;

import at.tu.dago.core.psql.model.Graph;
import org.mapstruct.Mapper;

import java.util.List;

import static at.tu.dago.core.grpc.GraphProtos.GraphResponse;

@Mapper(componentModel = "spring")
public interface GraphMapper {

	default GraphResponse graphResponse(List<Graph> graphs) {
		GraphResponse.Builder builder = GraphResponse.newBuilder();
		graphs.forEach(graph -> builder.addGraph(graphResponseGraph(graph)));

		return builder.build();
	}

	default GraphResponse.Graph graphResponseGraph(Graph graph) {
		return GraphResponse.Graph.newBuilder()
								  .setId(graph.getId())
								  .setName(graph.getName())
								  .setDescription(graph.getDescription())
								  .build();
	}
}
