package at.tu.dago.web.api;

import at.tu.dago.core.client.GrpcClientPreprocessor;
import at.tu.dago.core.grpc.GraphPageServiceGrpc;
import at.tu.dago.core.redis.model.GraphPage;
import at.tu.dago.core.service.FileUploadService;
import at.tu.dago.core.service.GraphService;
import at.tu.dago.core.utils.Bencher;
import at.tu.dago.web.api.interceptor.LogInterceptor;
import at.tu.dago.web.mapper.GraphPageProtosMapper;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;
import java.util.UUID;

import static at.tu.dago.core.grpc.EmptyProto.Empty;
import static at.tu.dago.core.grpc.GraphPageProtos.*;

@GRpcService(interceptors = { LogInterceptor.class })
@Slf4j
public class GraphPageServiceImpl extends GraphPageServiceGrpc.GraphPageServiceImplBase {

	private GraphService graphService;
	private GraphPageProtosMapper mapper;
	private FileUploadService fileUploadService;
	private GrpcClientPreprocessor client;
	private Bencher bencher;

	@Autowired
	public GraphPageServiceImpl(GraphService graphService, GraphPageProtosMapper mapper, FileUploadService fileUploadService,
			GrpcClientPreprocessor client, Bencher bencher) {

		this.graphService = graphService;
		this.mapper = mapper;
		this.fileUploadService = fileUploadService;
		this.client = client;
		this.bencher = bencher;
	}

	@Override
	public void fetch(GraphPageRequest request, StreamObserver<GraphPageResponse> responseObserver) {
		bencher.start();
		Optional<GraphPage> graphPage = graphService.getGraphPage(request.getGraphId(), request.getPageNumber(),
																  request.getPageDepth());
		bencher.measure("Service fetch");

		GraphPageResponse response = graphPage.map(gp -> mapper.graphPageResponse(gp))
											  .orElse(mapper.emptyGraphPageResponse());

		responseObserver.onNext(response);
		responseObserver.onCompleted();
	}

	@Override
	public void getPageCount(PageCountRequest request, StreamObserver<PageCountResponse> responseObserver) {
		bencher.start();
		int count = graphService.countPagesOfGraph(request.getGraphId());

		PageCountResponse response = PageCountResponse.newBuilder()
													  .setCounter(count)
													  .build();
		bencher.measure("Service getPageCount");

		responseObserver.onNext(response);
		responseObserver.onCompleted();
	}

	@Override
	public void getPageDepth(PageDepthRequest request, StreamObserver<PageDepthResponse> responseObserver) {
		bencher.start();
		int depth = graphService.depthOfPage(request.getGraphId(), request.getPageNumber());

		PageDepthResponse response = PageDepthResponse.newBuilder()
													  .setDepth(depth)
													  .build();
		bencher.measure("Service getPageDepth");

		responseObserver.onNext(response);
		responseObserver.onCompleted();
	}

	@Override
	public void initUpload(InitUploadRequest request, StreamObserver<InitUploadResponse> responseObserver) {
		String uniqueId = UUID.randomUUID()
							  .toString();

		// TODO move the cache clearing into the upload-finalization-method (after fixing the gRPC-client call)
		// Clear the lifetime-cache for the current graph_page, since the database-values might change during pre-processing
		// leading to invalid values stored in the cache!
		graphService.clearLifetimeCacheOfLastPage(request.getGraphId());

		fileUploadService.initUpload(uniqueId, request.getGraphId(), request.getName(), request.getType(), request.getSize());

		InitUploadResponse response = InitUploadResponse.newBuilder()
														.setId(uniqueId)
														.build();

		responseObserver.onNext(response);
		responseObserver.onCompleted();
	}

	@Override
	public void chunkUpload(ChunkUploadRequest request, StreamObserver<Empty> responseObserver) {
		// Convert encoded byte string into regular string
		String chunk = request.getChunk();

		if (request.getNumber() > 0) {
			fileUploadService.cacheChunk(request.getId(), request.getNumber(), chunk, chunk.length());
		} else {
			log.error("Chunk number must be larger than zero - ignoring this chunk.");
		}

		responseObserver.onNext(Empty.newBuilder()
									 .build());
		responseObserver.onCompleted();
	}

	@Override
	public void finalizeUpload(FinalizeUploadRequest request, StreamObserver<FinalizeUploadResponse> responseObserver) {
		boolean result = fileUploadService.finalizeUpload(request.getId(), request.getNumberOfChunks());

		if (result) {
			result = client.call(request.getId());
			log.debug("Preprocessor result is '{}'", result);

			graphService.clearCache();
		} else {
			log.warn("Could not finalize update");
		}

		FinalizeUploadResponse response = FinalizeUploadResponse.newBuilder()
																.setResult(result)
																.build();

		responseObserver.onNext(response);
		responseObserver.onCompleted();
	}
}
