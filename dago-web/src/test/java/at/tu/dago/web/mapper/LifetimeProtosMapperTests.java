package at.tu.dago.web.mapper;

import at.tu.dago.core.grpc.LifetimeProto;
import at.tu.dago.core.redis.model.Lifetimes;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LifetimeProtosMapperTests {

	@Autowired
	private LifetimeProtosMapper mapper;

	@Test
	public void lifetimeResponse_returnLifetimeResponse_ifValidParameters() {
		// given
		Lifetimes lifetimes = new Lifetimes();
		lifetimes.setIds(Arrays.asList(1L, 2L, 3L));
		lifetimes.setValues(Arrays.asList(11, 22, 33));

		// when
		LifetimeProto.LifetimeResponse response = mapper.lifetimeResponse(lifetimes);

		// then
		// @formatter:off
		assertThat(response.getLifetimes(), is(not(nullValue())));
		assertThat(response.getLifetimes().getIdsCount(), is(3));
		assertThat(response.getLifetimes().getIdsList().get(0), is(1L));
		assertThat(response.getLifetimes().getIdsList().get(1), is(2L));
		assertThat(response.getLifetimes().getIdsList().get(2), is(3L));
		assertThat(response.getLifetimes().getValuesCount(), is(3));
		assertThat(response.getLifetimes().getValuesList().get(0), is(11));
		assertThat(response.getLifetimes().getValuesList().get(1), is(22));
		assertThat(response.getLifetimes().getValuesList().get(2), is(33));
		// @formatter:on
	}
}
