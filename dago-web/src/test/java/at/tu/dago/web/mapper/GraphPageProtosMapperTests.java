package at.tu.dago.web.mapper;

import at.tu.dago.core.redis.model.GraphPage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static at.tu.dago.core.grpc.GraphPageProtos.GraphPageResponse;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GraphPageProtosMapperTests {

	@Autowired
	private GraphPageProtosMapper mapper;

	@Test
	public void graphPageResponse_returnGraphPageResponse_ifValidParameters() {
		// given
		GraphPage graphPage = new GraphPage();
		graphPage.setGraphId(123L);
		graphPage.setPageNumber(5);
		graphPage.setPageDepth(1);
		graphPage.setPageId(456L);
		graphPage.setNodeIds(Arrays.asList(11D, 22D, 33D));
		graphPage.setEdgeIds(Arrays.asList(44D, 55D));
		graphPage.setNodes(Arrays.asList(1f, 2f, 3f, 4f));
		graphPage.setEdges(Arrays.asList(5f, 6f, 7f, 8f));

		// when
		GraphPageResponse response = mapper.graphPageResponse(graphPage);

		// then
		// @formatter:off
		assertThat(response.getGraphPage(), is(not(nullValue())));
		assertThat(response.getGraphPage().getGraphId(), is(123L));
		assertThat(response.getGraphPage().getPageNumber(), is(5));
		assertThat(response.getGraphPage().getPageDepth(), is(1));
		assertThat(response.getGraphPage().getPageId(), is(456L));

		assertThat(response.getGraphPage().getNodeIdList().size(), is(3));
		assertThat(response.getGraphPage().getNodeIdList().get(0), is(11D));
		assertThat(response.getGraphPage().getNodeIdList().get(1), is(22D));
		assertThat(response.getGraphPage().getNodeIdList().get(2), is(33D));

		assertThat(response.getGraphPage().getEdgeIdList().size(), is(2));
		assertThat(response.getGraphPage().getEdgeIdList().get(0), is(44D));
		assertThat(response.getGraphPage().getEdgeIdList().get(1), is(55D));

		assertThat(response.getGraphPage().getNodeList().size(), is(4));
		assertThat(response.getGraphPage().getNodeList().get(0), is(1f));
		assertThat(response.getGraphPage().getNodeList().get(1), is(2f));
		assertThat(response.getGraphPage().getNodeList().get(2), is(3f));
		assertThat(response.getGraphPage().getNodeList().get(3), is(4f));

		assertThat(response.getGraphPage().getEdgeList().size(), is(4));
		assertThat(response.getGraphPage().getEdgeList().get(0), is(5f));
		assertThat(response.getGraphPage().getEdgeList().get(1), is(6f));
		assertThat(response.getGraphPage().getEdgeList().get(2), is(7f));
		assertThat(response.getGraphPage().getEdgeList().get(3), is(8f));
		// @formatter:on
	}
}
