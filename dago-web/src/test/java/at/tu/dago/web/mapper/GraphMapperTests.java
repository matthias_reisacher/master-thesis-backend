package at.tu.dago.web.mapper;

import at.tu.dago.core.psql.model.Graph;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static at.tu.dago.core.grpc.GraphProtos.GraphResponse;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GraphMapperTests {

	@Autowired
	private GraphMapper mapper;

	@Test
	public void graphResponse_returnGraphResponse_ifValidParameters() {
		// given
		Graph graph1 = new Graph("Foo", "Bar", null);
		graph1.setId(1L);

		Graph graph2 = new Graph("Baz", "Qux", null);
		graph2.setId(2L);

		List<Graph> graphs = new ArrayList<>();
		graphs.add(graph1);
		graphs.add(graph2);

		// when
		GraphResponse response = mapper.graphResponse(graphs);

		// then
		// @formatter:off
		assertThat(response.getGraphList().size(), is(2));

		assertThat(response.getGraph(0).getId(), is(1L));
		assertThat(response.getGraph(0).getName(), is("Foo"));
		assertThat(response.getGraph(0).getDescription(), is("Bar"));

		assertThat(response.getGraph(1).getId(), is(2L));
		assertThat(response.getGraph(1).getName(), is("Baz"));
		assertThat(response.getGraph(1).getDescription(), is("Qux"));
		// @formatter:on
	}
}
