package at.tu.dago.core.storage;

import at.tu.dago.core.psql.dao.EdgeDAO;
import at.tu.dago.core.psql.dao.NodeDAO;
import at.tu.dago.core.psql.dao.PageDAO;
import at.tu.dago.core.psql.model.*;
import at.tu.dago.core.redis.dao.GraphPageDAO;
import at.tu.dago.core.redis.mapper.GraphPageMapper;
import at.tu.dago.core.redis.model.GraphPage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class GraphPageStorageTests {

	@InjectMocks
	private GraphPageStorage storage;

	@Mock
	private PageDAO pageDaoMock;
	@Mock
	private NodeDAO nodeDaoMock;
	@Mock
	private EdgeDAO edgeDaoMock;
	@Mock
	private GraphPageDAO graphPageDAOMock;
	@Mock
	private GraphPageMapper mapperMock;

	@Before
	public void before() {
		// Setup test data
		Graph graph = new Graph("Foo", "Bar", null);
		graph.setId(1L);

		Page page = new Page(graph, 1, 1, null);

		Position pos1 = new Position(1f, 2f);
		Position pos2 = new Position(3f, 4f);

		Node node1 = new Node(page, 1, true, pos1, new Attribute());
		Node node2 = new Node(page, 1, true, pos2, new Attribute());

		List<Node> nodes = new ArrayList<>();
		nodes.add(node1);
		nodes.add(node2);

		List<Edge> edges = new ArrayList<>();
		edges.add(new Edge(page, 5, node1, node2, new Attribute()));

		GraphPage graphPage = new GraphPage();
		graphPage.setGraphId(graph.getId());
		graphPage.setPageNumber(page.getNumber());
		graphPage.setPageDepth(1);
		graphPage.setNodes(Arrays.asList(1f, 2f, 3f, 4f));
		graphPage.setEdges(Arrays.asList(5f, 6f, 7f, 8f));

		// Setup mocks
		given(pageDaoMock.findByGraphAndNumber(graph.getId(), 1)).willReturn(Optional.of(page));
		given(nodeDaoMock.findAllByPageAndDepth(page.getId(), 1)).willReturn(nodes);
		given(edgeDaoMock.findAllByPageAndDepth(page.getId(), 1)).willReturn(edges);
		given(graphPageDAOMock.findByPageProperties(graphPage.getGraphId(), graphPage.getPageNumber(),
													graphPage.getPageDepth())).willReturn(Optional.of(graphPage));
		given(mapperMock.graphPage(page, nodes, edges, 1)).willReturn(graphPage);
	}

	@Test
	public void getGraphPageOfDepthFromDatabase_returnGraphPage_ifPageOfGraphForDepthFound() {
		// when
		Optional<GraphPage> graphPage = storage.getGraphPageOfDepthFromDatabase(1L, 1, 1);

		// then
		assertThat(graphPage.isPresent(), is(true));
		assertThat(graphPage.get()
							.getGraphId(), is(1L));
	}

	@Test
	public void getGraphPageOfDepthFromDatabase_returnEmptyOptional_ifGraphNotFound() {
		// when
		Optional<GraphPage> graphPage = storage.getGraphPageOfDepthFromDatabase(2L, 1, 1);

		// then
		assertThat(graphPage.isPresent(), is(false));
	}

	@Test
	public void getGraphPageOfDepthFromDatabase_returnEmptyOptional_ifPageNotFound() {
		// when
		Optional<GraphPage> graphPage = storage.getGraphPageOfDepthFromDatabase(1L, 2, 1);

		// then
		assertThat(graphPage.isPresent(), is(false));
	}

	@Test
	public void getGraphPageOfDepthFromDatabase_returnEmptyOptional_ifDepthTooLarge() {
		// when
		Optional<GraphPage> graphPage = storage.getGraphPageOfDepthFromDatabase(1L, 1, 2);

		// then
		assertThat(graphPage.isPresent(), is(false));
	}

	@Test
	public void getGraphPageOfDepthFromCache_returnGraphPage_ifPageOfGraphForDepthFound() {
		// when
		Optional<GraphPage> graphPage = storage.getGraphPageOfDepthFromCache(1L, 1, 1);

		// then
		assertThat(graphPage.isPresent(), is(true));
		assertThat(graphPage.get()
							.getGraphId(), is(1L));
	}

	@Test
	public void getGraphPageOfDepthFromCache_returnEmptyOptional_ifGraphNotFound() {
		// when
		Optional<GraphPage> graphPage = storage.getGraphPageOfDepthFromCache(2L, 1, 1);

		// then
		assertThat(graphPage.isPresent(), is(false));
	}

	@Test
	public void getGraphPageOfDepthFromCache_returnEmptyOptional_ifPageNotFound() {
		// when
		Optional<GraphPage> graphPage = storage.getGraphPageOfDepthFromCache(1L, 2, 1);

		// then
		assertThat(graphPage.isPresent(), is(false));
	}

	@Test
	public void getGraphPageOfDepthFromCache_returnEmptyOptional_ifDepthTooLarge() {
		// when
		Optional<GraphPage> graphPage = storage.getGraphPageOfDepthFromCache(1L, 1, 2);

		// then
		assertThat(graphPage.isPresent(), is(false));
	}

	@Test
	public void cacheGraphPage_callSaveOfGraphPageDAO_ifGraphPageIsCached() {
		// given
		GraphPage graphPage = new GraphPage();
		graphPage.setGraphId(1);
		graphPage.setPageNumber(1);
		graphPage.setPageDepth(1);

		given(graphPageDAOMock.save(graphPage)).willReturn(graphPage);

		// when
		storage.cacheGraphPage(graphPage);

		// then
		then(graphPageDAOMock).should(times(1))
							  .save(graphPage);
	}

	@Test
	public void clearCache_callDeleteAllOfGraphPageDAO_ifCacheIsCleared() {
		// when
		storage.clearCache();

		// then
		then(graphPageDAOMock).should(times(1))
							  .deleteAll();
	}
}
