package at.tu.dago.core.psql.dao;

import at.tu.dago.core.psql.model.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class NodeDAOIT {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private NodeDAO dao;

	/* Test data */
	private static Long pageId;

	@Before
	public void before() {
		Date now = new Date();

		Graph graph = new Graph("Test Graph", "Contains arbitrary data for testing.", now);
		entityManager.persist(graph);

		Page page = new Page(graph, 1, 2, now);
		pageId = entityManager.persistAndGetId(page, Long.class);

		Position pos1 = new Position(-4f, 4f);
		entityManager.persist(pos1);

		Position pos2 = new Position(4f, 4f);
		entityManager.persist(pos2);

		Position pos3 = new Position(4f, -4f);
		entityManager.persist(pos3);

		Position pos4 = new Position(-4f, -4f);
		entityManager.persist(pos4);

		Position pos5 = new Position(-4f, -4f);
		entityManager.persist(pos5);

		Attribute attr1 = new Attribute(null, 10, "Stuff", 1);
		entityManager.persist(attr1);

		Attribute attr2 = new Attribute(null, 1, "More stuff", -1);
		entityManager.persist(attr2);

		Attribute attr3 = new Attribute(11);
		entityManager.persist(attr3);

		Attribute attr4 = new Attribute(12);
		entityManager.persist(attr4);

		Attribute attr5 = new Attribute(13);
		entityManager.persist(attr5);

		entityManager.persist(new Node(page, 1, true, pos1, attr1));
		entityManager.persist(new Node(page, 1, false, pos2, attr2));
		entityManager.persist(new Node(page, 2, true, pos3, attr3));
		entityManager.persist(new Node(page, 2, true, pos4, attr4));
		entityManager.persist(new Node(page, 2, true, pos5, attr5));

		entityManager.flush();
	}

	@Test
	public void findAllByPageAndMaxDepth_returnTwoNodes_ifDepthIsOne() {
		// when
		List<Node> nodes = dao.findAllByPageAndMaxDepth(pageId, 1);

		// then
		assertThat(nodes.size(), is(2));
	}

	@Test
	public void findAllByPageAndMaxDepth_returnFourNodes_ifDepthIsTwo() {
		// when
		List<Node> nodes = dao.findAllByPageAndMaxDepth(pageId, 2);

		// then
		assertThat(nodes.size(), is(4));
	}

	@Test
	public void findAllByPageAndMaxDepth_returnFourNodes_ifDepthIsThree() {
		// when
		List<Node> nodes = dao.findAllByPageAndMaxDepth(pageId, 3);

		// then
		assertThat(nodes.size(), is(4));
	}

	@Test
	public void findAllByPageAndMaxDepth_returnEmptyList_ifPageNotFound() {
		// when
		List<Node> nodes = dao.findAllByPageAndMaxDepth(pageId + 1, 3);

		// then
		assertThat(nodes.size(), is(0));
	}

	@Test
	public void findAllByPageAndDepth_returnTwoNodes_ifDepthIsOne() {
		// when
		List<Node> nodes = dao.findAllByPageAndDepth(pageId, 1);

		// then
		assertThat(nodes.size(), is(2));
	}

	@Test
	public void findAllByPageAndDepth_returnFourNodes_ifDepthIsTwo() {
		// when
		List<Node> nodes = dao.findAllByPageAndDepth(pageId, 2);

		// then
		assertThat(nodes.size(), is(3));
	}

	@Test
	public void findAllByHierarchy_returnTwoNodes_ifDepthIsTwo() {
		// when
		List<Node> nodes = dao.findAllByHierarchy(pageId, Arrays.asList(1L, 13L));

		// then
		assertThat(nodes.size(), is(2));
	}

	@Test
	public void findAllWithLifetime_returnTwoNodes_ifPageFound() {
		// when
		List<Node> nodes = dao.findAllWithLifetime(pageId);

		// then
		assertThat(nodes.size(), is(2));
	}
}
