package at.tu.dago.core.service;

import at.tu.dago.core.redis.dao.FileUploadChunkDAO;
import at.tu.dago.core.redis.dao.FileUploadMetadataDAO;
import at.tu.dago.core.redis.model.FileUploadChunk;
import at.tu.dago.core.redis.model.FileUploadMetadata;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import utils.TestBoundValueOperations;

import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class FileUploadServiceTests {

	@InjectMocks
	private FileUploadService service;

	@Mock
	private FileUploadMetadataDAO metadataDAO;

	@Mock
	private FileUploadChunkDAO chunkDAO;

	@Mock
	private StringRedisTemplate jedis;

	@Test
	public void initUpload_callSaveOfFileUploadMetadataDAO() {
		// when
		service.initUpload("123", 1L, "test", "something", 456L);

		// then
		// @formatter:off
		then(metadataDAO).should(times(1)).save(any());
		// @formatter:on
	}

	@Test
	public void cacheChunk_callSaveOfFileUploadChunkDAO() {
		// when
		service.cacheChunk("123", 456, "something", 789);

		// then
		// @formatter:off
		then(chunkDAO).should(times(1)).save(any());
		// @formatter:on
	}

	@Test
	public void finalizeUpload_returnTrue_ifMetadataAndChunksFoundInCache() {
		// given
		FileUploadMetadata metadata = new FileUploadMetadata();
		metadata.setId("123");
		metadata.setSize(1000L);

		FileUploadChunk chunk1 = new FileUploadChunk();
		chunk1.setSize(750);
		chunk1.setContent("foo");

		FileUploadChunk chunk2 = new FileUploadChunk();
		chunk2.setSize(250);
		chunk2.setContent("bar");

		Iterable<FileUploadChunk> chunks = Arrays.asList(chunk1, chunk2);

		given(metadataDAO.findById("123")).willReturn(Optional.of(metadata));
		given(chunkDAO.findAllById(Arrays.asList("123"))).willReturn(chunks);
		given(jedis.boundValueOps(anyString())).willReturn(new TestBoundValueOperations<>());

		// when
		boolean result = service.finalizeUpload("123");

		// then
		// @formatter:off
		assertThat(result, is(true));
		then(metadataDAO).should(times(1)).findById(any());
		then(chunkDAO).should(times(1)).findAllById(any());
		then(jedis).should(times(2)).boundValueOps(any());

		then(metadataDAO).should(times(1)).deleteById(any());
		then(chunkDAO).should(times(1)).deleteAll(any());
		// @formatter:on
	}

	@Test
	public void finalizeUpload_returnFalse_ifMetadataNotFoundInCache() {
		// when
		boolean result = service.finalizeUpload("123");

		// then
		// @formatter:off
		assertThat(result, is(false));
		then(metadataDAO).should(times(1)).findById(any());
		then(chunkDAO).should(never()).findAllById(any());
		then(jedis).should(never()).boundValueOps(any());
		// @formatter:on
	}

	@Test
	public void finalizeUpload_returnFalse_ifFileIsNotComplete() {
		// given
		FileUploadMetadata metadata = new FileUploadMetadata();
		metadata.setId("123");
		metadata.setSize(1000L);

		FileUploadChunk chunk1 = new FileUploadChunk();
		chunk1.setSize(500);
		chunk1.setContent("foo");

		FileUploadChunk chunk2 = new FileUploadChunk();
		chunk2.setSize(499);
		chunk2.setContent("bar");

		Iterable<FileUploadChunk> chunks = Arrays.asList(chunk1, chunk2);

		given(metadataDAO.findById("123")).willReturn(Optional.of(metadata));
		given(chunkDAO.findAllById(Arrays.asList("123"))).willReturn(chunks);

		// when
		boolean result = service.finalizeUpload("123");

		// then
		// @formatter:off
		assertThat(result, is(false));
		then(metadataDAO).should(times(1)).findById(any());
		then(chunkDAO).should(times(1)).findAllById(any());
		then(jedis).should(never()).boundValueOps(any());
		// @formatter:on
	}
}
