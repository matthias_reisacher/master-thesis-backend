package at.tu.dago.core.redis.mapper;

import at.tu.dago.core.psql.model.*;
import at.tu.dago.core.redis.model.GraphPage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GraphPageMapperTests {

	@Autowired
	private GraphPageMapper mapper;

	@Test
	public void graphPage_returnGraphPage_ifValidParameters() {
		// given
		Graph graph = new Graph("foo", "bar", null);
		graph.setId(123L);

		Page page = new Page(graph, 5, 1, null);
		page.setId(456L);

		Position pos1 = new Position(1f, 2f);
		Position pos2 = new Position(3f, 4f);

		Node node1 = new Node(page, 5, true, pos1, new Attribute());
		node1.setId(11L);
		Node node2 = new Node(page, 1, true, pos2, new Attribute());
		node2.setId(22L);

		List<Node> nodes = new ArrayList<>();
		nodes.add(node1);
		nodes.add(node2);

		Edge edge = new Edge(page, 5, node2, node1, new Attribute());
		edge.setId(33L);

		List<Edge> edges = new ArrayList<>();
		edges.add(edge);

		// when
		GraphPage graphPage = mapper.graphPage(page, nodes, edges, 1);

		// then
		// @formatter:off
		assertThat(graphPage.getGraphId(), is(123L));
		assertThat(graphPage.getPageNumber(), is(5));
		assertThat(graphPage.getPageDepth(), is(1));
		assertThat(graphPage.getPageId(), is(456L));

		assertThat(graphPage.getNodeIds().size(), is(2));
		assertThat(graphPage.getNodeIds().get(0), is(11D));
		assertThat(graphPage.getNodeIds().get(1), is(22D));

		assertThat(graphPage.getEdgeIds().size(), is(1));
		assertThat(graphPage.getEdgeIds().get(0), is(33D));

		assertThat(graphPage.getNodes().size(), is(4));
		assertThat(graphPage.getNodes().get(0), is(1f));
		assertThat(graphPage.getNodes().get(1), is(2f));
		assertThat(graphPage.getNodes().get(2), is(3f));
		assertThat(graphPage.getNodes().get(3), is(4f));

		assertThat(graphPage.getEdges().size(), is(4));
		assertThat(graphPage.getEdges().get(0), is(3f));
		assertThat(graphPage.getEdges().get(1), is(4f));
		assertThat(graphPage.getEdges().get(2), is(1f));
		assertThat(graphPage.getEdges().get(3), is(2f));
		// @formatter:on
	}
}
