package at.tu.dago.core.psql.dao;

import at.tu.dago.core.psql.model.Graph;
import at.tu.dago.core.psql.model.Page;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PageDAOIT {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private PageDAO dao;

	/* Test data */
	private static Long graphId;

	@Before
	public void before() {
		Date now = new Date();

		Graph graph = new Graph("Test Graph", "Contains arbitrary data for testing.", now);
		graphId = entityManager.persistAndGetId(graph, Long.class);

		entityManager.persist(new Page(graph, 1, 1, now));
		entityManager.persist(new Page(graph, 2, 1, now));
	}

	@Test
	public void findByGraphAndNumber_returnPageOne_ifPageNumberOne() {
		// when
		Optional<Page> page = dao.findByGraphAndNumber(graphId, 1);

		// then
		assertThat(page.isPresent(), is(true));
		assertThat(1, is(page.get().getNumber()));
	}

	@Test
	public void findByGraphAndNumber_returnPageTwo_ifPageNumberTwo() {
		// when
		Optional<Page> page = dao.findByGraphAndNumber(graphId, 2);

		// then
		assertThat(page.isPresent(), is(true));
		assertThat(2, is(page.get().getNumber()));
	}

	@Test
	public void findByGraphAndNumber_returnEmptyOptional_ifPageNumberThree() {
		// when
		Optional<Page> page = dao.findByGraphAndNumber(graphId, 3);

		// then
		assertThat(page.isPresent(), is(false));
	}

	@Test
	public void findByGraphAndNumber_returnEmptyOptional_ifGraphNotFound() {
		// when
		Optional<Page> page = dao.findByGraphAndNumber(graphId, 3);

		// then
		assertThat(page.isPresent(), is(false));
	}
}
