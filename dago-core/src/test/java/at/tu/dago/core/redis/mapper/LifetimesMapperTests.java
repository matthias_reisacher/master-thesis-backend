package at.tu.dago.core.redis.mapper;

import at.tu.dago.core.psql.model.*;
import at.tu.dago.core.redis.model.Lifetimes;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LifetimesMapperTests {

	@Autowired
	private LifetimesMapper mapper;

	@Test
	public void nodeLifetimes_returnLifetimes_ifValidParameters() {
		// given
		Graph graph = new Graph("foo", "bar", null);
		graph.setId(123L);

		Page page = new Page(graph, 5, 1, null);
		page.setId(456L);

		Position pos1 = new Position(1f, 2f);
		Position pos2 = new Position(3f, 4f);

		Attribute attr1 = new Attribute(1L, 1L, "Something", 1);
		Attribute attr2 = new Attribute(2L, 2L, "Else", -1);

		Node node1 = new Node(page, 5, true, pos1, attr1);
		node1.setId(41L);
		Node node2 = new Node(page, 1, true, pos2, attr2);
		node2.setId(42L);

		List<Node> nodes = new ArrayList<>();
		nodes.add(node1);
		nodes.add(node2);

		// when
		Lifetimes nodeLifetimes = mapper.nodeLifetimes(page.getId(), nodes);

		// then
		// @formatter:off
		assertThat(nodeLifetimes.getType(), is(Lifetimes.ELEMENT_TYPE.Nodes));
		assertThat(nodeLifetimes.getPageId(), is(456L));
		assertThat(nodeLifetimes.getIds().size(), is(2));
		assertThat(nodeLifetimes.getIds().get(0), is(41L));
		assertThat(nodeLifetimes.getIds().get(1), is(42L));
		assertThat(nodeLifetimes.getValues().size(), is(2));
		assertThat(nodeLifetimes.getValues().get(0), is(1));
		assertThat(nodeLifetimes.getValues().get(1), is(-1));
		// @formatter:on
	}

	@Test
	public void edgeLifetimes_returnLifetimes_ifValidParameters() {
		// given
		Graph graph = new Graph("foo", "bar", null);
		graph.setId(123L);

		Page page = new Page(graph, 5, 1, null);
		page.setId(456L);

		Position pos1 = new Position(1f, 2f);
		Position pos2 = new Position(3f, 4f);
		Position pos3 = new Position(5f, 6f);
		Position pos4 = new Position(7f, 8f);

		Node node1 = new Node(page, 1, true, pos1, new Attribute());
		Node node2 = new Node(page, 1, true, pos2, new Attribute());
		Node node3 = new Node(page, 2, true, pos3, new Attribute());
		Node node4 = new Node(page, 2, true, pos3, new Attribute());

		Attribute attr1 = new Attribute();
		attr1.setLifetime(1);
		Attribute attr2 = new Attribute();
		attr2.setLifetime(-1);

		Edge edge1 = new Edge(page, 1, node1, node2, attr1);
		edge1.setId(41L);
		Edge edge2 = new Edge(page, 2, node3, node4, attr2);
		edge2.setId(42L);

		List<Edge> edges = new ArrayList<>();
		edges.add(edge1);
		edges.add(edge2);

		// when
		Lifetimes nodeLifetimes = mapper.edgeLifetimes(page.getId(), edges);

		// then
		// @formatter:off
		assertThat(nodeLifetimes.getType(), is(Lifetimes.ELEMENT_TYPE.Edges));
		assertThat(nodeLifetimes.getPageId(), is(456L));
		assertThat(nodeLifetimes.getIds().size(), is(2));
		assertThat(nodeLifetimes.getIds().get(0), is(41L));
		assertThat(nodeLifetimes.getIds().get(1), is(42L));
		assertThat(nodeLifetimes.getValues().size(), is(2));
		assertThat(nodeLifetimes.getValues().get(0), is(1));
		assertThat(nodeLifetimes.getValues().get(1), is(-1));
		// @formatter:on
	}
}
