package at.tu.dago.core.redis.mapper;

import at.tu.dago.core.psql.model.Attribute;
import at.tu.dago.core.redis.model.NodeAttributes;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NodeAttributesMapperTests {

	@Autowired
	private NodeAttributeMapper mapper;

	@Test
	public void nodeHierarchy_returnHierarchyResponse_ifValidParameters() {
		// given
		Attribute attribute = new Attribute(null, 0, "Some attributes", 0);

		// when
		NodeAttributes nodeAttributes = mapper.nodeAttribute(123L, attribute);

		// then
		// @formatter:off
		assertThat(nodeAttributes.getId(), is("123"));
		assertThat(nodeAttributes.getAttributes(), is("Some attributes"));
		// @formatter:on
	}
}
