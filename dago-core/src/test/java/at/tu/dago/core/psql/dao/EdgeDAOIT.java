package at.tu.dago.core.psql.dao;

import at.tu.dago.core.psql.model.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EdgeDAOIT {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private EdgeDAO dao;

	/* Test data */
	private static Long pageId;

	@Before
	public void before() {
		Date now = new Date();

		Graph graph = new Graph("Test Graph", "Contains arbitrary data for testing.", now);
		entityManager.persist(graph);

		Page page = new Page(graph, 1, 1, now);
		pageId = entityManager.persistAndGetId(page, Long.class);

		Position pos1 = new Position(-4f, 4f);
		entityManager.persist(pos1);

		Position pos2 = new Position(4f, 4f);
		entityManager.persist(pos2);

		Position pos3 = new Position(4f, -4f);
		entityManager.persist(pos3);

		Position pos4 = new Position(-4f, -4f);
		entityManager.persist(pos4);

		Attribute attr1 = new Attribute(null, 0, "{}", 1);
		entityManager.persist(attr1);

		Attribute attr2 = new Attribute(null, 0, "{}", -1);
		entityManager.persist(attr2);

		Attribute attr3 = new Attribute();
		entityManager.persist(attr3);

		Attribute attr4 = new Attribute();
		entityManager.persist(attr4);

		Attribute attr5 = new Attribute();
		entityManager.persist(attr5);

		Node node1 = new Node(page, 1, true, pos1, attr1);
		entityManager.persist(node1);

		Node node2 = new Node(page, 1, true, pos2, attr2);
		entityManager.persist(node2);

		Node node3 = new Node(page, 1, true, pos2, attr2);
		entityManager.persist(node3);

		Node node4 = new Node(page, 1, true, pos2, attr2);
		entityManager.persist(node4);

		entityManager.persist(new Edge(page, 1, node1, node2, attr1));
		entityManager.persist(new Edge(page, 1, node3, node4, attr2));
		entityManager.persist(new Edge(page, 2, node1, node3, attr3));
		entityManager.persist(new Edge(page, 2, node2, node4, attr4));
		entityManager.persist(new Edge(page, 2, node3, node4, attr5));

		entityManager.flush();
	}

	@Test
	public void findAllByPageAndDepth_returnTwoEdges_ifDepthIsOne() {
		// when
		List<Edge> edges = dao.findAllByPageAndDepth(pageId, 1);

		// then
		assertThat(2, is(edges.size()));
	}

	@Test
	public void findAllByPageAndDepth_returnThreeEdges_ifDepthIsTwo() {
		// when
		List<Edge> edges = dao.findAllByPageAndDepth(pageId, 2);

		// then
		assertThat(3, is(edges.size()));
	}

	@Test
	public void findAllByPageAndDepth_returnEmptyList_ifDepthIsThree() {
		// when
		List<Edge> edges = dao.findAllByPageAndDepth(pageId, 3);

		// then
		assertThat(0, is(edges.size()));
	}

	@Test
	public void findAllByPageAndDepth_returnEmptyList_ifPageNotFound() {
		// when
		List<Edge> edges = dao.findAllByPageAndDepth(pageId + 1, 1);

		// then
		assertThat(0, is(edges.size()));
	}

	@Test
	public void findAllWithLifetime_returnTwoEdges_ifPageFound() {
		// when
		List<Edge> edge = dao.findAllWithLifetime(pageId);

		// then
		assertThat(edge.size(), is(2));
	}
}
