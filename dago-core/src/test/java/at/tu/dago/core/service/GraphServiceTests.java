package at.tu.dago.core.service;

import at.tu.dago.core.psql.dao.GraphDAO;
import at.tu.dago.core.psql.model.Graph;
import at.tu.dago.core.redis.model.GraphPage;
import at.tu.dago.core.storage.GraphPageStorage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class GraphServiceTests {

	@InjectMocks
	private GraphService service;

	@Mock
	private GraphPageStorage storage;

	@Mock
	private GraphDAO graphDAO;

	@Before
	public void before() {
		GraphPage graphPage1 = new GraphPage();
		graphPage1.setGraphId(1);
		graphPage1.setPageNumber(1);
		graphPage1.setPageDepth(1);

		GraphPage graphPage2 = new GraphPage();
		graphPage2.setGraphId(2);
		graphPage2.setPageNumber(2);
		graphPage2.setPageDepth(2);

		// graphPage1 only in cache
		given(storage.getGraphPageOfDepthFromCache(graphPage1.getGraphId(), graphPage1.getPageNumber(),
												   graphPage1.getPageDepth())).willReturn(Optional.of(graphPage1));

		// graphPage2 only in database
		given(storage.getGraphPageOfDepthFromCache(graphPage2.getGraphId(), graphPage2.getPageNumber(),
												   graphPage2.getPageDepth())).willReturn(Optional.empty());
		given(storage.getGraphPageOfDepthFromDatabase(graphPage2.getGraphId(), graphPage2.getPageNumber(),
													  graphPage2.getPageDepth())).willReturn(Optional.of(graphPage2));

		/* ***** */

		List<Graph> graphs = new ArrayList<>();

		Graph graph1 = new Graph("Foo", "Bar", null);
		graph1.setId(1L);
		graphs.add(graph1);

		Graph graph2 = new Graph("Baz", "Qux", null);
		graph2.setId(2L);
		graphs.add(graph2);

		given(graphDAO.findAllByOrderByTimestampAsc()).willReturn(graphs);
	}

	@Test
	public void getGraphPage_returnGraphPage1_ifGraphPageIsCached() {
		// when
		Optional<GraphPage> graphPage = service.getGraphPage(1L, 1, 1);

		// then
		// @formatter:off
		assertThat(graphPage.isPresent(), is(true));
		assertThat(graphPage.get().getGraphId(), is(1L));
		assertThat(graphPage.get().getPageNumber(), is(1));
		assertThat(graphPage.get().getPageDepth(), is(1));
		then(storage).should(never()).getGraphPageOfDepthFromDatabase(1L, 1, 1);
		// @formatter:on
	}

	@Test
	public void getGraphPage_returnGraphPage2_ifGraphPageIsNotCachedButStored() {
		// when
		Optional<GraphPage> graphPage = service.getGraphPage(2L, 2, 2);

		// then
		// @formatter:off
		assertThat(graphPage.isPresent(), is(true));
		assertThat(graphPage.get().getGraphId(), is(2L));
		assertThat(graphPage.get().getPageNumber(), is(2));
		assertThat(graphPage.get().getPageDepth(), is(2));
		then(storage).should(times(1)).getGraphPageOfDepthFromCache(2L, 2, 2);
		then(storage).should(times(1)).cacheGraphPage(any());
		// @formatter:on
	}

	@Test
	public void getGraphPage_returnEmptyOptional_ifGraphPageIsNotCachedAndNotStored() {
		// when
		Optional<GraphPage> graphPage = service.getGraphPage(3L, 3, 3);

		// then
		assertThat(graphPage.isPresent(), is(false));
		then(storage).should(times(1))
					 .getGraphPageOfDepthFromCache(3L, 3, 3);
		then(storage).should(times(1))
					 .getGraphPageOfDepthFromDatabase(3L, 3, 3);
		then(storage).should(never())
					 .cacheGraphPage(any());
	}

	@Test
	public void getGraphs_returnListOfGraphs_ifGraphsWhereFound() {
		// when
		List<Graph> graphs = service.getGraphs();

		// then
		assertThat(graphs.size(), is(2));
	}
}
