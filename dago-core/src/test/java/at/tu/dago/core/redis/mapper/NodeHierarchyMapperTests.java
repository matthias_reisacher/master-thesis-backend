package at.tu.dago.core.redis.mapper;

import at.tu.dago.core.psql.model.Node;
import at.tu.dago.core.redis.model.NodeHierarchy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NodeHierarchyMapperTests {

	@Autowired
	private NodeHierarchyMapper mapper;

	@Test
	public void nodeHierarchy_returnHierarchyResponse_ifValidParameters() {
		// given
		Node node1 = new Node(null, 1, false, null, null);
		node1.setId(1L);

		Node node2 = new Node(null, 2, true, null, null);
		node2.setId(2L);

		List<Node> nodes = new ArrayList<>();
		nodes.add(node1);
		nodes.add(node2);

		// when
		NodeHierarchy nodeHierarchy = mapper.nodeHierarchy(123L, nodes);

		// then
		// @formatter:off
		assertThat(nodeHierarchy.getId(), is("123"));
		assertThat(nodeHierarchy.getNodeIds().size(), is(2));

		assertThat(nodeHierarchy.getNodeIds().get(0), is(1L));
		assertThat(nodeHierarchy.getNodeIds().get(1), is(2L));
		// @formatter:on
	}
}
