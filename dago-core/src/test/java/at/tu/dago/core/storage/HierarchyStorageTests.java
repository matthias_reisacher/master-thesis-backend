package at.tu.dago.core.storage;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class HierarchyStorageTests {

	@InjectMocks
	private HierarchyStorage storage;

	@Test
	public void getPredecessorsOfQuadTreeId_returnSevenPredecessors_forQuadTreeIdOfDepthSeven() {
		// when
		List<Long> predecessors = storage.createPredecessorsOfQuadTreeId(4212334L);

		// then
		assertThat(predecessors.size(), is(6));
		assertThat(predecessors.get(0), is(4L));
		assertThat(predecessors.get(1), is(42L));
		assertThat(predecessors.get(2), is(421L));
		assertThat(predecessors.get(3), is(4212L));
		assertThat(predecessors.get(4), is(42123L));
		assertThat(predecessors.get(5), is(421233L));
	}

	@Test
	public void getSuccessorsOfQuadTreeIdsUntilDepth_returnEightyFourSuccessors_forMaxDepthOfFive() {
		// when
		List<Long> successors = storage.createSuccessorsOfQuadTreeIdsUntilDepth(42L, 5);

		// then
		assertThat(successors.size(), is(84));
		assertThat(successors.get(0), is(421L));
		assertThat(successors.get(1), is(4211L));
		assertThat(successors.get(2), is(42111L));
		assertThat(successors.get(3), is(42112L));
		assertThat(successors.get(4), is(42113L));
		assertThat(successors.get(5), is(42114L));
		assertThat(successors.get(6), is(4212L));
		assertThat(successors.get(7), is(42121L));
		assertThat(successors.get(8), is(42122L));
		assertThat(successors.get(9), is(42123L));
		assertThat(successors.get(10), is(42124L));
		assertThat(successors.get(11), is(4213L));
		assertThat(successors.get(12), is(42131L));
		assertThat(successors.get(13), is(42132L));
		assertThat(successors.get(14), is(42133L));
		assertThat(successors.get(15), is(42134L));
		assertThat(successors.get(16), is(4214L));
		assertThat(successors.get(17), is(42141L));
		assertThat(successors.get(18), is(42142L));
		assertThat(successors.get(19), is(42143L));
		assertThat(successors.get(20), is(42144L));
		assertThat(successors.get(21), is(422L));
		assertThat(successors.get(22), is(4221L));
		assertThat(successors.get(23), is(42211L));
		assertThat(successors.get(24), is(42212L));
		assertThat(successors.get(25), is(42213L));
		assertThat(successors.get(26), is(42214L));
		assertThat(successors.get(27), is(4222L));
		assertThat(successors.get(28), is(42221L));
		assertThat(successors.get(29), is(42222L));
		assertThat(successors.get(30), is(42223L));
		assertThat(successors.get(31), is(42224L));
		assertThat(successors.get(32), is(4223L));
		assertThat(successors.get(33), is(42231L));
		assertThat(successors.get(34), is(42232L));
		assertThat(successors.get(35), is(42233L));
		assertThat(successors.get(36), is(42234L));
		assertThat(successors.get(37), is(4224L));
		assertThat(successors.get(38), is(42241L));
		assertThat(successors.get(39), is(42242L));
		assertThat(successors.get(40), is(42243L));
		assertThat(successors.get(41), is(42244L));
		assertThat(successors.get(42), is(423L));
		assertThat(successors.get(43), is(4231L));
		assertThat(successors.get(44), is(42311L));
		assertThat(successors.get(45), is(42312L));
		assertThat(successors.get(46), is(42313L));
		assertThat(successors.get(47), is(42314L));
		assertThat(successors.get(48), is(4232L));
		assertThat(successors.get(49), is(42321L));
		assertThat(successors.get(50), is(42322L));
		assertThat(successors.get(51), is(42323L));
		assertThat(successors.get(52), is(42324L));
		assertThat(successors.get(53), is(4233L));
		assertThat(successors.get(54), is(42331L));
		assertThat(successors.get(55), is(42332L));
		assertThat(successors.get(56), is(42333L));
		assertThat(successors.get(57), is(42334L));
		assertThat(successors.get(58), is(4234L));
		assertThat(successors.get(59), is(42341L));
		assertThat(successors.get(60), is(42342L));
		assertThat(successors.get(61), is(42343L));
		assertThat(successors.get(62), is(42344L));
		assertThat(successors.get(63), is(424L));
		assertThat(successors.get(64), is(4241L));
		assertThat(successors.get(65), is(42411L));
		assertThat(successors.get(66), is(42412L));
		assertThat(successors.get(67), is(42413L));
		assertThat(successors.get(68), is(42414L));
		assertThat(successors.get(69), is(4242L));
		assertThat(successors.get(70), is(42421L));
		assertThat(successors.get(71), is(42422L));
		assertThat(successors.get(72), is(42423L));
		assertThat(successors.get(73), is(42424L));
		assertThat(successors.get(74), is(4243L));
		assertThat(successors.get(75), is(42431L));
		assertThat(successors.get(76), is(42432L));
		assertThat(successors.get(77), is(42433L));
		assertThat(successors.get(78), is(42434L));
		assertThat(successors.get(79), is(4244L));
		assertThat(successors.get(80), is(42441L));
		assertThat(successors.get(81), is(42442L));
		assertThat(successors.get(82), is(42443L));
		assertThat(successors.get(83), is(42444L));
	}

	@Test
	public void getSuccessorsOfQuadTreeIdsUntilDepth_returnEmptyList_forMaxDepthTooSmall() {
		// when
		List<Long> successors = storage.createSuccessorsOfQuadTreeIdsUntilDepth(423124L, 6);

		// then
		assertThat(successors.size(), is(0));
	}

	@Test
	public void findQuadTreeIdsOfHierarchy_return() {
		// when
		List<Long> hierarchy = storage.findQuadTreeIdsOfHierarchy(42L, 3);

		// then
		assertThat(hierarchy.size(), is(6));
		assertThat(hierarchy.get(0), is(4L));
		assertThat(hierarchy.get(1), is(42L));
		assertThat(hierarchy.get(2), is(421L));
		assertThat(hierarchy.get(3), is(422L));
		assertThat(hierarchy.get(4), is(423L));
		assertThat(hierarchy.get(5), is(424L));
	}
}
