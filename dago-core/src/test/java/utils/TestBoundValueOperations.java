package utils;

import org.springframework.data.redis.connection.DataType;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisOperations;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class TestBoundValueOperations<K, V> implements BoundValueOperations<K, V> {

	@Override
	public void set(V value) {
		
	}

	@Override
	public void set(V value, long timeout, TimeUnit unit) {

	}

	@Override
	public Boolean setIfAbsent(V value) {
		return null;
	}

	@Override
	public Boolean setIfAbsent(V value, long timeout, TimeUnit unit) {
		return null;
	}

	@Override
	public Boolean setIfPresent(V value) {
		return null;
	}

	@Override
	public Boolean setIfPresent(V value, long timeout, TimeUnit unit) {
		return null;
	}

	@Override
	public V get() {
		return null;
	}

	@Override
	public V getAndSet(V value) {
		return null;
	}

	@Override
	public Long increment() {
		return null;
	}

	@Override
	public Long increment(long delta) {
		return null;
	}

	@Override
	public Double increment(double delta) {
		return null;
	}

	@Override
	public Long decrement() {
		return null;
	}

	@Override
	public Long decrement(long delta) {
		return null;
	}

	@Override
	public Integer append(String value) {
		return null;
	}

	@Override
	public String get(long start, long end) {
		return null;
	}

	@Override
	public void set(V value, long offset) {

	}

	@Override
	public Long size() {
		return null;
	}

	@Override
	public RedisOperations<K, V> getOperations() {
		return null;
	}

	@Override
	public K getKey() {
		return null;
	}

	@Override
	public DataType getType() {
		return null;
	}

	@Override
	public Long getExpire() {
		return null;
	}

	@Override
	public Boolean expire(long timeout, TimeUnit unit) {
		return null;
	}

	@Override
	public Boolean expireAt(Date date) {
		return null;
	}

	@Override
	public Boolean persist() {
		return null;
	}

	@Override
	public void rename(K newKey) {

	}
}
