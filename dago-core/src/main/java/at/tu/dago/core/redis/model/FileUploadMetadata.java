package at.tu.dago.core.redis.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.json.JSONObject;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;

@RedisHash("FileUploadMetadata")
@Setter
@Getter
@NoArgsConstructor
@ToString
public class FileUploadMetadata implements Serializable {

	@Id
	private String id;
	private long graphId;
	private String name;
	private String type;
	private long size;

	public String toJSON() {
		return new JSONObject().put("id", id)
							   .put("graphId", graphId)
							   .put("name", name)
							   .put("type", type)
							   .put("size", size)
							   .toString();
	}
}
