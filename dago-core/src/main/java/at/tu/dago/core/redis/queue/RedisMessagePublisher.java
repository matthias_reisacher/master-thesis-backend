package at.tu.dago.core.redis.queue;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RedisMessagePublisher implements MessagePublisher {

	private RedisTemplate<String, Object> redisTemplate;
	private ChannelTopic topic;

	@Autowired
	public RedisMessagePublisher(RedisTemplate<String, Object> redisTemplate, ChannelTopic topic) {
		this.redisTemplate = redisTemplate;
		this.topic = topic;
	}

	public void publish(String message) {
		log.debug("Sending message: {}, {}", topic.getTopic(), message);
		redisTemplate.convertAndSend(topic.getTopic(), message);
	}
}
