package at.tu.dago.core.redis.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;

@RedisHash("NodeAttributes")
@Setter
@Getter
@NoArgsConstructor
public class NodeAttributes implements Serializable {

	@Id
	private String id;
	private String attributes;

	@Override
	public String toString() {
		return String.format("NodeAttributes(id=%s, attributes=%s)", id, attributes);
	}
}
