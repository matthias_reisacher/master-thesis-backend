package at.tu.dago.core.redis.model;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.List;

@RedisHash("Lifetimes")
@Getter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Slf4j
public class Lifetimes implements Serializable {

	public enum ELEMENT_TYPE {
		Nodes,
		Edges
	}

	@Id
	@EqualsAndHashCode.Exclude
	private String id;    // Redis hash

	@Setter
	private ELEMENT_TYPE type;

	@Setter
	private Long pageId;

	@Setter
	@EqualsAndHashCode.Exclude
	private List<Long> ids;

	@Setter
	@EqualsAndHashCode.Exclude
	private List<Integer> values;

	/**
	 * Sets {@link Lifetimes#id} to the calculated redis hash.
	 */
	public void calcId() {
		this.id = calcRedisHash();
	}

	/**
	 * Creates the hash code from {@link Lifetimes#type} and {@link Lifetimes#pageId}.
	 */
	public String calcRedisHash() {
		if (type == null || pageId == 0) {
			log.warn("Hash code of Lifetimes is calculated with unset values!");
		}

		return Integer.valueOf(this.hashCode())
					  .toString();
	}

	public Lifetimes(ELEMENT_TYPE type, Long pageId) {
		this.type = type;
		this.pageId = pageId;
	}

	public String identifiersToString() {
		return String.format("%s (type=%s, pageId=%d)", this.getClass()
															.getSimpleName(), type.toString(), pageId);
	}
}
