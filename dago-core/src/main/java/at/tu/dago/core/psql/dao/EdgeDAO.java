package at.tu.dago.core.psql.dao;

import at.tu.dago.core.psql.model.Edge;
import at.tu.dago.core.psql.model.Node;
import at.tu.dago.core.psql.model.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface EdgeDAO extends JpaRepository<Edge, Long> {

	@Query("SELECT e FROM Edge e WHERE e.page.id = :page_id AND e.depth = :depth")
	List<Edge> findAllByPageAndDepth(@Param("page_id") long pageId, @Param("depth") int depth);

	@Query("SELECT e FROM Edge e WHERE e.page.id = :page_id AND e.attribute.lifetime <> 0")
	List<Edge> findAllWithLifetime(@Param("page_id") long pageId);

	@Query("SELECT e.id, e.startNode.position, e.endNode.position FROM Edge e WHERE e.page.id = :page_id AND e.depth = :depth")
	List<Object[]> findAllEdgeObjectsByPageAndDepth(@Param("page_id") long pageId, @Param("depth") int depth);

	default List<Edge> findAllPartiallyByPageAndDepth(@Param("page_id") long pageId, @Param("depth") int depth) {
		List<Edge> edges = new ArrayList<>();

		List<Object[]> data = findAllEdgeObjectsByPageAndDepth(pageId, depth);
		data.forEach(objects -> {
			Node start = new Node();
			start.setDepth(depth);
			start.setPosition((Position) objects[1]);

			Node end = new Node();
			end.setDepth(depth);
			end.setPosition((Position) objects[2]);

			Edge edge = new Edge();
			edge.setId((Long) objects[0]);
			edge.setDepth(depth);
			edge.setStartNode(start);
			edge.setEndNode(end);

			edges.add(edge);
		});

		return edges;
	}
}
