package at.tu.dago.core.psql.dao;

import at.tu.dago.core.psql.model.Graph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GraphDAO extends JpaRepository<Graph, Long> {

	List<Graph> findAllByOrderByTimestampAsc();
}
