package at.tu.dago.core.redis.queue;

public interface MessagePublisher {

	void publish(String message);
}
