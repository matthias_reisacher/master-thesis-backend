package at.tu.dago.core.redis.mapper;

import at.tu.dago.core.psql.model.Edge;
import at.tu.dago.core.psql.model.Node;
import at.tu.dago.core.psql.model.Page;
import at.tu.dago.core.psql.model.PsqlModel;
import at.tu.dago.core.redis.model.GraphPage;
import org.mapstruct.Mapper;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
public interface GraphPageMapper {

	default GraphPage graphPage(Page page, List<Node> nodes, List<Edge> edges, int depth) {
		GraphPage graphPage = new GraphPage();
		graphPage.setGraphId(page.getGraph()
								 .getId());
		graphPage.setPageNumber(page.getNumber());
		graphPage.setPageDepth(depth);
		graphPage.setPageId(page.getId());
		graphPage.setNodeIds(modelIds(nodes));
		graphPage.setEdgeIds(modelIds(edges));
		graphPage.setNodes(nodesPositions(nodes));
		graphPage.setEdges(edgesPositions(edges));

		return graphPage;
	}

	default <T extends PsqlModel> List<Double> modelIds(List<T> models) {
		List<Double> ids = new ArrayList<>();
		models.forEach(model -> ids.add((double) model.getId()));

		return ids;
	}

	default List<Float> nodesPositions(List<Node> nodes) {
		List<Float> positions = new ArrayList<>();
		nodes.forEach(node -> {
			positions.add(node.getPosition()
							  .getX());
			positions.add(node.getPosition()
							  .getY());
		});

		return positions;
	}

	default List<Float> edgesPositions(List<Edge> edges) {
		List<Float> positions = new ArrayList<>();
		edges.forEach(edge -> {
			positions.add(edge.getStartNode()
							  .getPosition()
							  .getX());
			positions.add(edge.getStartNode()
							  .getPosition()
							  .getY());

			positions.add(edge.getEndNode()
							  .getPosition()
							  .getX());
			positions.add(edge.getEndNode()
							  .getPosition()
							  .getY());
		});

		return positions;
	}
}
