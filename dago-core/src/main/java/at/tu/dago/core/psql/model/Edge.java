package at.tu.dago.core.psql.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;

@Entity
@Data
@NoArgsConstructor
public class Edge implements PsqlModel {

	@Id
	@GeneratedValue
	private long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "page_id")
	private Page page;

	@Min(0)
	private int depth;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "start_node_id")
	private Node startNode;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "end_node_id")
	private Node endNode;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "attribute_id")
	private Attribute attribute;

	public Edge(Page page, @Min(0) int depth, Node startNode, Node endNode, Attribute attribute) {
		this.page = page;
		this.depth = depth;
		this.startNode = startNode;
		this.endNode = endNode;
		this.attribute = attribute;
	}
}
