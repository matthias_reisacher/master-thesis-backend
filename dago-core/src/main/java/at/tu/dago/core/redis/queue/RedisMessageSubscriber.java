package at.tu.dago.core.redis.queue;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Getter
@Slf4j
public class RedisMessageSubscriber implements MessageListener {

	private static List<String> messageList = new ArrayList<>();

	public void onMessage(Message message, byte[] pattern) {
		messageList.add(message.toString());
		log.debug("Message received: {}", new String(message.getBody()));
	}
}
