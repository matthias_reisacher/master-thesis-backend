package at.tu.dago.core.psql.dao;

import at.tu.dago.core.psql.model.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AttributeDAO extends JpaRepository<Position, Long> {

}
