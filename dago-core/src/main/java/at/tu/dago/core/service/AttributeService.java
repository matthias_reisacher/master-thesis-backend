package at.tu.dago.core.service;

import at.tu.dago.core.psql.model.Attribute;
import at.tu.dago.core.redis.mapper.NodeAttributeMapper;
import at.tu.dago.core.redis.model.Lifetimes;
import at.tu.dago.core.redis.model.NodeAttributes;
import at.tu.dago.core.storage.AttributeStorage;
import at.tu.dago.core.utils.Bencher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class AttributeService {

	private AttributeStorage storage;
	private NodeAttributeMapper mapper;
	private Bencher bencher;

	@Autowired
	public AttributeService(AttributeStorage storage, NodeAttributeMapper mapper, Bencher bencher) {
		this.storage = storage;
		this.mapper = mapper;
		this.bencher = bencher;
	}

	public String getAttributes(long nodeId) {
		Optional<NodeAttributes> cachedAttributes = storage.getNodeAttributesFromCache(nodeId);

		String attributes = "";
		if (!cachedAttributes.isPresent()) {
			Optional<Attribute> attribute = storage.getNodeAttributeFromDatabase(nodeId);

			if (attribute.isPresent() && attribute.get()
												  .getAttributes() != null) {
				NodeAttributes nodeAttributes = mapper.nodeAttribute(nodeId, attribute.get());

				// Store object inside cache for future access
				storage.cacheNodeAttributes(nodeAttributes);

				attributes = nodeAttributes.getAttributes();
			} else {
				log.warn("Could not find any attribute for node {}", nodeId);
			}
		} else {
			attributes = cachedAttributes.get()
										 .getAttributes();
		}

		return attributes;
	}

	public Optional<Lifetimes> getNodeLifetimes(long pageId) {
		bencher.start();
		Optional<Lifetimes> lifetimes = storage.getNodeLifetimesFromCache(pageId);
		bencher.measure("Load node lifetimes from cache");

		if (!lifetimes.isPresent()) {
			lifetimes = storage.getNodeLifetimesFromDatabase(pageId);
			bencher.measure("Load node lifetimes from database");

			// Store object inside cache for future access
			lifetimes.ifPresent(lt -> storage.cacheLifetimes(lt));
			bencher.measure("Store node lifetimes in cache");
		}

		return lifetimes;
	}

	public Optional<Lifetimes> getEdgeLifetimes(long pageId) {
		bencher.start();
		Optional<Lifetimes> lifetimes = storage.getEdgeLifetimesFromCache(pageId);
		bencher.measure("Load edge lifetimes from cache");

		if (!lifetimes.isPresent()) {
			lifetimes = storage.getEdgeLifetimesFromDatabase(pageId);
			bencher.measure("Load edge lifetimes from database");

			// Store object inside cache for future access
			lifetimes.ifPresent(lt -> storage.cacheLifetimes(lt));
			bencher.measure("Store edge lifetimes in cache");
		}

		return lifetimes;
	}
}
