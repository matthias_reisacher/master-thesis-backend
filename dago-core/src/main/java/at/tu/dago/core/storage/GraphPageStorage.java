package at.tu.dago.core.storage;

import at.tu.dago.core.psql.dao.EdgeDAO;
import at.tu.dago.core.psql.dao.NodeDAO;
import at.tu.dago.core.psql.dao.PageDAO;
import at.tu.dago.core.psql.model.Edge;
import at.tu.dago.core.psql.model.Node;
import at.tu.dago.core.psql.model.Page;
import at.tu.dago.core.redis.dao.GraphPageDAO;
import at.tu.dago.core.redis.mapper.GraphPageMapper;
import at.tu.dago.core.redis.model.GraphPage;
import at.tu.dago.core.utils.Bencher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@Slf4j
public class GraphPageStorage {

	private PageDAO pageDAO;
	private NodeDAO nodeDAO;
	private EdgeDAO edgeDAO;

	private GraphPageDAO graphPageDAO;

	private GraphPageMapper graphPageMapper;

	private Bencher bencher;

	@Autowired
	public GraphPageStorage(PageDAO pageDAO, NodeDAO nodeDAO, EdgeDAO edgeDAO, GraphPageDAO graphPageDAO,
			GraphPageMapper graphPageMapper, Bencher bencher) {

		this.pageDAO = pageDAO;
		this.nodeDAO = nodeDAO;
		this.edgeDAO = edgeDAO;
		this.graphPageDAO = graphPageDAO;
		this.graphPageMapper = graphPageMapper;
		this.bencher = bencher;
	}

	public Optional<GraphPage> getGraphPageOfDepthFromDatabase(long graphId, int pageNumber, int depth) {
		bencher.start();
		Optional<Page> page = pageDAO.findByGraphAndNumber(graphId, pageNumber);
		bencher.measure("Load graph page from database");

		if (!page.isPresent()) {
			log.warn("Could not fetch Page with graph_id={} and number={} from DB", graphId, pageNumber);
			return Optional.empty();
		} else if (depth > page.get()
							   .getMaxDepth()) {
			log.warn("Depth={} is larger than the maximum depth of page={} - abort DB fetch.", depth, page.get()
																										  .getMaxDepth());
			return Optional.empty();
		}

		List<Node> nodes = nodeDAO.findAllPartiallyByPageAndDepth(page.get()
																	  .getId(), depth);
		bencher.measure("Load nodes of graph page from database");
		List<Edge> edges = edgeDAO.findAllPartiallyByPageAndDepth(page.get()
																	  .getId(), depth);
		bencher.measure("Load edges of graph page from database");

		GraphPage graphPage = graphPageMapper.graphPage(page.get(), nodes, edges, depth);
		bencher.measure("Map graph page");
		log.debug("Fetched from DB: {}", graphPage.identifiersToString());

		return Optional.of(graphPage);
	}

	public Optional<GraphPage> getGraphPageOfDepthFromCache(long graphId, int pageNumber, int depth) {
		Optional<GraphPage> graphPage = graphPageDAO.findByPageProperties(graphId, pageNumber, depth);

		if (graphPage.isPresent()) {
			log.debug("Fetched from Cache: {}", graphPage.get()
														 .identifiersToString());
		} else {
			log.debug("Could not find GraphPage with parameters graph_id={}, number={}, depth={} in cache.", graphId,
					  pageNumber, depth);
		}

		return graphPage;
	}

	public Integer countPagesOfGraph(long graphId) {
		return pageDAO.countPagesOfGraph(graphId);
	}

	public Optional<Page> findLastPageOfGraph(long graphId) {
		Integer maxPageNumber = pageDAO.maxPageNumberOfGraph(graphId);

		if (maxPageNumber != null) {
			return pageDAO.findByGraphAndNumber(graphId, maxPageNumber);
		} else {
			return Optional.empty();
		}
	}

	public Optional<Integer> depthOfPage(long graphId, int pageNumber) {
		Optional<Page> page = pageDAO.findByGraphAndNumber(graphId, pageNumber);
		return page.map(Page::getMaxDepth);
	}

	public void cacheGraphPage(GraphPage graphPage) {
		graphPage.calcId();
		log.debug("Cached: GraphPage with identifier = {}", graphPageDAO.save(graphPage)
																		.identifiersToString());
	}

	public void clearCache() {
		graphPageDAO.deleteAll();
		log.debug("Cached for GraphPage cleared");
	}
}
