package at.tu.dago.core.storage;

import at.tu.dago.core.psql.dao.NodeDAO;
import at.tu.dago.core.psql.model.Node;
import at.tu.dago.core.psql.model.Page;
import at.tu.dago.core.redis.dao.NodeHierarchyDAO;
import at.tu.dago.core.redis.model.NodeHierarchy;
import at.tu.dago.core.utils.Bencher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.PrimitiveIterator;

// TODO outsource to pre-processor service and replace here with gRPC call!
@Service
@Slf4j
public class HierarchyStorage {

	private NodeDAO nodeDAO;
	private NodeHierarchyDAO nodeHierarchyDAO;

	private Bencher bencher;

	@Autowired
	public HierarchyStorage(NodeDAO nodeDAO, NodeHierarchyDAO nodeHierarchyDAO, Bencher bencher) {
		this.nodeDAO = nodeDAO;
		this.nodeHierarchyDAO = nodeHierarchyDAO;
		this.bencher = bencher;
	}

	public Optional<NodeHierarchy> getHierarchyFromCache(long nodeId) {
		return nodeHierarchyDAO.findById(String.valueOf(nodeId));
	}

	public void cacheNodeHierarchy(NodeHierarchy nodeHierarchy) {
		log.debug("Cached: NodeHierarchy with ID = {}", nodeHierarchyDAO.save(nodeHierarchy)
																		.getId());
	}

	public void clearCache() {
		nodeHierarchyDAO.deleteAll();
		log.debug("Cached for NodeHierarchy cleared");
	}

	public List<Node> getHierarchyFromDatabase(long nodeId) {
		Optional<Node> node = nodeDAO.findById(nodeId);

		List<Node> hierarchy = new ArrayList();
		if (node.isPresent()) {
			Page page = node.get()
							.getPage();

			long quadTreeId = node.get()
								  .getAttribute()
								  .getQuadTreeId();

			List<Long> hierarchyIds = findQuadTreeIdsOfHierarchy(quadTreeId, page.getMaxDepth());

			bencher.start();
			hierarchy.addAll(nodeDAO.findAllByHierarchy(page.getId(), hierarchyIds));
			bencher.measure("Load hierarchy from database");
		}

		return hierarchy;
	}

	public List<Long> findQuadTreeIdsOfHierarchy(long quadTreeId, int maxDepth) {
		List<Long> hierarchy = new ArrayList<>();

		bencher.start();
		hierarchy.addAll(createPredecessorsOfQuadTreeId(quadTreeId));
		hierarchy.add(quadTreeId);
		hierarchy.addAll(createSuccessorsOfQuadTreeIdsUntilDepth(quadTreeId, maxDepth));
		bencher.measure("Hierarchy find " + hierarchy.size() + " quadtree ids");

		return hierarchy;
	}

	public List<Long> createPredecessorsOfQuadTreeId(long quadTreeId) {
		List<Long> predecessors = new ArrayList<>();

		String stringId = String.valueOf(quadTreeId);
		PrimitiveIterator.OfInt digits = stringId.chars()
												 .map(Character::getNumericValue)
												 .iterator();

		while (digits.hasNext()) {
			StringBuilder predecessorBuilder = new StringBuilder();

			// Append last digits
			int n_predecessors = predecessors.size();
			if (n_predecessors > 0) {
				predecessorBuilder.append(predecessors.get(n_predecessors - 1));
			}

			// Append new digit
			predecessorBuilder.append(digits.nextInt());

			predecessors.add(Long.parseLong(predecessorBuilder.toString()));
		}

		// Remove last value
		if (predecessors.size() > 0) {
			predecessors = predecessors.subList(0, predecessors.size() - 1);
		}

		return predecessors;
	}

	public List<Long> createSuccessorsOfQuadTreeIdsUntilDepth(long quadTreeId, int maxDepth) {
		List<Long> successors = new ArrayList<>();

		String stringId = String.valueOf(quadTreeId);
		if (stringId.length() < maxDepth) {
			String successorBase = String.valueOf(quadTreeId);

			for (int i = 1; i <= 4; i++) {
				long successor = Long.parseLong(successorBase + i);
				successors.add(successor);

				// Call recursively
				successors.addAll(createSuccessorsOfQuadTreeIdsUntilDepth(successor, maxDepth));
			}
		}

		return successors;
	}
}
