package at.tu.dago.core.psql.dao;

import at.tu.dago.core.psql.model.Node;
import at.tu.dago.core.psql.model.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface NodeDAO extends JpaRepository<Node, Long> {

	// Logic for the creation and selection of edges must be improved and adopted to make this work
	@Query("SELECT n FROM Node n WHERE n.page.id = :page_id AND (n.depth = :depth OR (n.leaf = true AND n.depth < :depth))")
	List<Node> findAllByPageAndMaxDepth(@Param("page_id") long pageId, @Param("depth") int maxDepth);

	@Query("SELECT n FROM Node n WHERE n.page.id = :page_id AND n.depth = :depth")
	List<Node> findAllByPageAndDepth(@Param("page_id") long pageId, @Param("depth") int depth);

	@Query("SELECT n FROM Node n WHERE n.page.id = :page_id AND n.attribute.quadTreeId in (:quad_tree_ids)")
	List<Node> findAllByHierarchy(@Param("page_id") long pageId, @Param("quad_tree_ids") List<Long> quadTreeIds);

	@Query("SELECT n FROM Node n WHERE n.page.id = :page_id AND n.attribute.lifetime <> 0")
	List<Node> findAllWithLifetime(@Param("page_id") long pageId);

	@Query("SELECT n.id, n.position FROM Node n WHERE n.page.id = :page_id AND n.depth = :depth")
	List<Object[]> findAllNodeObjectsByPageAndDepth(@Param("page_id") long pageId, @Param("depth") int depth);

	default List<Node> findAllPartiallyByPageAndDepth(@Param("page_id") long pageId, @Param("depth") int depth) {
		List<Node> nodes = new ArrayList<>();

		List<Object[]> data = findAllNodeObjectsByPageAndDepth(pageId, depth);
		data.forEach(objects -> {
			Node node = new Node();
			node.setId((Long) objects[0]);
			node.setDepth(depth);
			node.setPosition((Position) objects[1]);

			nodes.add(node);
		});

		return nodes;
	}
}
