package at.tu.dago.core.redis.mapper;

import at.tu.dago.core.psql.model.Attribute;
import at.tu.dago.core.redis.model.NodeAttributes;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface NodeAttributeMapper {

	default NodeAttributes nodeAttribute(long nodeId, Attribute attribute) {
		NodeAttributes nodeAttributes = new NodeAttributes();
		nodeAttributes.setId(String.valueOf(nodeId));
		nodeAttributes.setAttributes(attribute.getAttributes());

		return nodeAttributes;
	}
}
