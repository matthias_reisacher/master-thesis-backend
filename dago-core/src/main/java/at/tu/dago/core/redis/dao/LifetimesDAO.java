package at.tu.dago.core.redis.dao;

import at.tu.dago.core.redis.model.Lifetimes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LifetimesDAO extends CrudRepository<Lifetimes, String> {

	default Optional<Lifetimes> findByTypeAndPage(Lifetimes.ELEMENT_TYPE type, long pageId) {
		Lifetimes lifetimes = new Lifetimes(type, pageId);
		return findById(lifetimes.calcRedisHash());
	}
}
