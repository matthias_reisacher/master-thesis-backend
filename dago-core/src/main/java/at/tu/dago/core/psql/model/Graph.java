package at.tu.dago.core.psql.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
public class Graph implements PsqlModel {

	@Id
	@GeneratedValue
	private long id;

	@Column(unique = true)
	@NotNull
	@Size(min = 1, max = 60)
	private String name;

	@NotNull
	@Size(min = 1, max = 150)
	private String description;

	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	private Date timestamp;

	public Graph(@NotNull @Size(min = 1, max = 30) String name, @NotNull @Size(min = 1, max = 150) String description,
			Date timestamp) {

		this.name = name;
		this.description = description;
		this.timestamp = timestamp;
	}
}
