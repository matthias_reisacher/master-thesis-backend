package at.tu.dago.core.psql.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
public class Page implements PsqlModel {

	@Id
	@GeneratedValue
	private long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "graph_id")
	private Graph graph;

	@Column(unique = true)
	@NotNull
	@Min(0)
	private int number;

	@Column(name = "max_depth")
	@NotNull
	@Min(0)
	private int maxDepth;

	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	private Date timestamp;

	public Page(@Min(0) Graph graph, @Min(0) int number, @Min(0) int maxDepth, Date timestamp) {
		this.graph = graph;
		this.number = number;
		this.maxDepth = maxDepth;
		this.timestamp = timestamp;
	}
}
