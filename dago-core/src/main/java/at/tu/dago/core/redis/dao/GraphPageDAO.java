package at.tu.dago.core.redis.dao;

import at.tu.dago.core.redis.model.GraphPage;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GraphPageDAO extends CrudRepository<GraphPage, String> {

	default Optional<GraphPage> findByPageProperties(long pageId, int pageNumber, int pageDepth) {
		GraphPage page = new GraphPage(pageId, pageNumber, pageDepth);
		return findById(page.calcRedisHash());
	}
}
