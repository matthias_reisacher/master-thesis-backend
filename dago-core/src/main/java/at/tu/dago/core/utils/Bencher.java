package at.tu.dago.core.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Service
@Scope("prototype")
@Validated
@Slf4j
public class Bencher {

	@Value("${dago.benchmark}")
	private int benchmark;

	private Long counter = null;

	public void start() {
		counter = System.currentTimeMillis();
	}

	public void measure(String msg) {
		if (benchmark > 0 && counter != null) {
			long duration = System.currentTimeMillis() - counter;
			log(msg + " : " + duration + "ms");
			start();
		}
	}

	public void log(String msg) {
		if (benchmark > 0 && msg != null) {
			log.error("[BENCH] {}", msg);
		}
	}
}
