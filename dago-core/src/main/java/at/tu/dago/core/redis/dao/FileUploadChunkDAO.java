package at.tu.dago.core.redis.dao;

import at.tu.dago.core.redis.model.FileUploadChunk;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

@Repository
public interface FileUploadChunkDAO extends CrudRepository<FileUploadChunk, String> {

	default Optional<FileUploadChunk> findByPageProperties(String uploadId, int number) {
		String id = FileUploadChunk.calcRedisHash(uploadId, number);
		return findById(id);
	}

	default List<FileUploadChunk> findByPageProperties(String uploadId, IntStream numbers) {
		List<FileUploadChunk> chunks = new ArrayList<>();
		numbers.forEach(number -> {
			findByPageProperties(uploadId, number).ifPresent(chunks::add);
		});

		return chunks;
	}
}
