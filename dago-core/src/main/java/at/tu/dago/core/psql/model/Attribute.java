package at.tu.dago.core.psql.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Data
public class Attribute implements PsqlModel {

	@Id
	@GeneratedValue
	private long id;

	@Column(name = "data_id")
	private Long dataId;

	@Column(name = "quad_tree")
	@NotNull
	@Min(0)
	private long quadTreeId;

	@NotNull
	private String attributes;

	@NotNull
	private int lifetime;

	// Minimum constructor for Edges
	public Attribute() {
		this.dataId = null;
		this.quadTreeId = 0;
		this.attributes = "{}";
		this.lifetime = 0;
	}

	// Minimum constructor for Nodes
	public Attribute(@NotNull @Min(0) int quadTreeId) {
		this.dataId = null;
		this.quadTreeId = quadTreeId;
		this.attributes = "{}";
		this.lifetime = 0;
	}

	public Attribute(Long dataId, @NotNull @Min(0) long quadTreeId, @NotNull String attributes, @NotNull int lifetime) {
		this.dataId = dataId;
		this.quadTreeId = quadTreeId;
		this.attributes = attributes;
		this.lifetime = lifetime;
	}
}
