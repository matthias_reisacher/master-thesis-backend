package at.tu.dago.core.storage;

import at.tu.dago.core.psql.dao.EdgeDAO;
import at.tu.dago.core.psql.dao.NodeDAO;
import at.tu.dago.core.psql.model.Attribute;
import at.tu.dago.core.psql.model.Edge;
import at.tu.dago.core.psql.model.Node;
import at.tu.dago.core.redis.dao.LifetimesDAO;
import at.tu.dago.core.redis.dao.NodeAttributesDAO;
import at.tu.dago.core.redis.mapper.LifetimesMapper;
import at.tu.dago.core.redis.model.Lifetimes;
import at.tu.dago.core.redis.model.NodeAttributes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class AttributeStorage {

	private NodeDAO nodeDAO;
	private EdgeDAO edgeDAO;
	private NodeAttributesDAO nodeAttributesDAO;
	private LifetimesDAO lifetimesDAO;

	private LifetimesMapper mapper;

	@Autowired
	public AttributeStorage(NodeDAO nodeDAO, EdgeDAO edgeDAO, NodeAttributesDAO nodeAttributesDAO, LifetimesDAO lifetimesDAO,
			LifetimesMapper mapper) {

		this.nodeDAO = nodeDAO;
		this.edgeDAO = edgeDAO;
		this.nodeAttributesDAO = nodeAttributesDAO;
		this.lifetimesDAO = lifetimesDAO;
		this.mapper = mapper;
	}

	public Optional<Attribute> getNodeAttributeFromDatabase(long nodeId) {
		Optional<Node> node = nodeDAO.findById(nodeId);

		return node.map(Node::getAttribute);
	}

	public Optional<NodeAttributes> getNodeAttributesFromCache(long nodeId) {
		return nodeAttributesDAO.findById(String.valueOf(nodeId));
	}

	public Optional<Lifetimes> getNodeLifetimesFromDatabase(long pageId) {
		List<Node> nodes = nodeDAO.findAllWithLifetime(pageId);

		if (nodes.isEmpty()) {
			log.warn("Could not fetch node-lifetimes of page={} from DB", pageId);
			return Optional.empty();
		}

		Lifetimes lifetimes = mapper.nodeLifetimes(pageId, nodes);
		log.debug("Fetched from DB: {}", lifetimes.identifiersToString());

		return Optional.of(lifetimes);
	}

	public Optional<Lifetimes> getEdgeLifetimesFromDatabase(long pageId) {
		List<Edge> edges = edgeDAO.findAllWithLifetime(pageId);

		if (edges.isEmpty()) {
			log.warn("Could not fetch edge-lifetimes of page={} from DB", pageId);
			return Optional.empty();
		}

		Lifetimes lifetimes = mapper.edgeLifetimes(pageId, edges);
		log.debug("Fetched from DB: {}", lifetimes.identifiersToString());

		return Optional.of(lifetimes);
	}

	public Optional<Lifetimes> getNodeLifetimesFromCache(long pageId) {
		return getLifetimesFromCache(Lifetimes.ELEMENT_TYPE.Nodes, pageId);
	}

	public Optional<Lifetimes> getEdgeLifetimesFromCache(long pageId) {
		return getLifetimesFromCache(Lifetimes.ELEMENT_TYPE.Edges, pageId);
	}

	private Optional<Lifetimes> getLifetimesFromCache(Lifetimes.ELEMENT_TYPE type, long pageId) {
		Optional<Lifetimes> lifetimes = lifetimesDAO.findByTypeAndPage(type, pageId);

		if (lifetimes.isPresent()) {
			log.debug("Fetched from Cache: {}", lifetimes.get()
														 .identifiersToString());
		} else {
			log.debug("Could not find Lifetimes with parameters type={}, pageid={} in cache.", type.toString(), pageId);
		}

		return lifetimes;
	}

	public void cacheNodeAttributes(NodeAttributes nodeHierarchy) {
		log.debug("Cached: cacheNodeAttributes with ID = {}", nodeAttributesDAO.save(nodeHierarchy)
																			   .getId());
	}

	public void cacheLifetimes(Lifetimes lifetimes) {
		lifetimes.calcId();
		log.debug("Cached: Lifetimes with identifier = {}", lifetimesDAO.save(lifetimes)
																		.identifiersToString());
	}

	public void clearLifetimesOfPage(long pageId) {
		// Remove node-lifetimes
		Lifetimes lifetimes = new Lifetimes(Lifetimes.ELEMENT_TYPE.Nodes, pageId);
		lifetimes.calcId();
		lifetimesDAO.delete(lifetimes);

		// Remove edge-lifetimes
		lifetimes.setType(Lifetimes.ELEMENT_TYPE.Edges);
		lifetimes.calcId();
		lifetimesDAO.delete(lifetimes);

		log.debug("Cache for Node- and Edge-Lifetimes of page with id={} cleared", pageId);
	}

	public void clearCache() {
		nodeAttributesDAO.deleteAll();
		lifetimesDAO.deleteAll();
		log.debug("Cache for NodeAttributes and Lifetimes cleared");
	}
}
