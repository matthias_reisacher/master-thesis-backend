package at.tu.dago.core.service;

import at.tu.dago.core.psql.model.Node;
import at.tu.dago.core.redis.mapper.NodeHierarchyMapper;
import at.tu.dago.core.redis.model.NodeHierarchy;
import at.tu.dago.core.storage.HierarchyStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HierarchyService {

	private HierarchyStorage storage;
	private NodeHierarchyMapper mapper;

	@Autowired
	public HierarchyService(HierarchyStorage storage, NodeHierarchyMapper mapper) {
		this.storage = storage;
		this.mapper = mapper;
	}

	public NodeHierarchy getNodeHierarchy(long nodeId) {
		Optional<NodeHierarchy> cachedHierarchy = storage.getHierarchyFromCache(nodeId);

		if (!cachedHierarchy.isPresent()) {
			List<Node> nodes = storage.getHierarchyFromDatabase(nodeId);

			NodeHierarchy nodeHierarchy = mapper.nodeHierarchy(nodeId, nodes);

			// Store object inside cache for future access
			storage.cacheNodeHierarchy(nodeHierarchy);

			return nodeHierarchy;
		} else {
			return cachedHierarchy.get();
		}
	}
}
