package at.tu.dago.core.psql.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
public class Position implements PsqlModel {

	@Id
	@GeneratedValue
	private long id;

	private float x;

	private float y;

	public Position(float x, float y) {
		this.x = x;
		this.y = y;
	}
}
