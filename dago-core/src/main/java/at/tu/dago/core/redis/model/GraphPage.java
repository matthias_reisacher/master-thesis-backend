package at.tu.dago.core.redis.model;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@RedisHash("GraphPage")
@Getter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Slf4j
public class GraphPage implements Serializable {

	@Id
	@EqualsAndHashCode.Exclude
	private String id;    // Redis hash

	@Setter
	private long graphId;
	@Setter
	private int pageNumber;
	@Setter
	private int pageDepth;

	@Setter
	@EqualsAndHashCode.Exclude
	private long pageId;

	@Setter
	@EqualsAndHashCode.Exclude
	private List<Double> nodeIds = new ArrayList<>();

	@Setter
	@EqualsAndHashCode.Exclude
	private List<Double> edgeIds = new ArrayList<>();

	@Setter
	@EqualsAndHashCode.Exclude
	private List<Float> nodes = new ArrayList<>();    // Tuple (x, y) of positions

	@Setter
	@EqualsAndHashCode.Exclude
	private List<Float> edges = new ArrayList<>();    // Tuple ((x, y), (x, y)) of start and end positions

	/**
	 * Sets {@link GraphPage#id} to the calculated redis hash.
	 */
	public void calcId() {
		this.id = calcRedisHash();
	}

	/**
	 * Creates the hash code from {@link GraphPage#graphId}, {@link GraphPage#pageNumber} and
	 * {@link GraphPage#pageDepth}.
	 */
	public String calcRedisHash() {
		if (graphId == 0 || pageNumber == 0 || pageDepth == 0) {
			log.warn("Hash code of GraphPage is calculated with unset values!");
		}

		return Integer.valueOf(this.hashCode())
					  .toString();
	}

	public GraphPage(long graphId, int pageNumber, int pageDepth) {
		this.graphId = graphId;
		this.pageNumber = pageNumber;
		this.pageDepth = pageDepth;
	}

	public String identifiersToString() {
		return String.format("%s (graphId=%d, pageNumber=%d, pageDepth=%d)", this.getClass()
																				 .getSimpleName(), graphId, pageNumber,
							 pageDepth);
	}
}
