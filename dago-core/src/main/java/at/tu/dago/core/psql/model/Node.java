package at.tu.dago.core.psql.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;

@Entity
@Data
@NoArgsConstructor
public class Node implements PsqlModel {

	@Id
	@GeneratedValue
	private long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "page_id")
	private Page page;

	@Min(0)
	private int depth;

	private boolean leaf;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "position_id")
	private Position position;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "attribute_id")
	private Attribute attribute;

	public Node(Page page, @Min(0) int depth, boolean leaf, Position position, Attribute attribute) {
		this.page = page;
		this.depth = depth;
		this.leaf = leaf;
		this.position = position;
		this.attribute = attribute;
	}
}
