package at.tu.dago.core.redis.dao;

import at.tu.dago.core.redis.model.FileUploadMetadata;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileUploadMetadataDAO extends CrudRepository<FileUploadMetadata, String> {

}
