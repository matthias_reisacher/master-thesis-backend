package at.tu.dago.core.redis.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@RedisHash("NodeHierarchy")
@Setter
@Getter
@NoArgsConstructor
public class NodeHierarchy implements Serializable {

	@Id
	private String id;
	private List<Long> nodeIds = new ArrayList<>();

	@Override
	public String toString() {
		return String.format("NodeHierarchy(id=%s, nNodeIds=%d)", id, nodeIds.size());
	}
}
