package at.tu.dago.core.client;

import at.tu.dago.core.grpc.ProcessFileProtos.ProcessFileRequest;
import at.tu.dago.core.grpc.ProcessFileProtos.ProcessFileResponse;
import at.tu.dago.core.grpc.ProcessFileServiceGrpc.ProcessFileServiceBlockingStub;
import io.grpc.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.concurrent.TimeUnit;

import static at.tu.dago.core.grpc.ProcessFileServiceGrpc.newBlockingStub;

@Service
@Validated
@Slf4j
public class GrpcClientPreprocessor {

	@Value("${dago.preprocessor.host}")
	@NotBlank
	private String host;

	@Value("${dago.preprocessor.port}")
	@Min(1025)
	@Max(65536)
	private int port;

	private static int DEADLINE = 60;

	private ManagedChannel channel;
	private ProcessFileServiceBlockingStub stub;

	@PostConstruct
	public void postConstruct() {
		log.info("Starting {} connected to {}:{}", this.getClass()
													   .getSimpleName(), host, port);

		channel = ManagedChannelBuilder.forAddress(host, port)
									   .usePlaintext()
									   .build();
		stub = newBlockingStub(channel);

		log.info("Setting deadline for client calls to {} minutes", DEADLINE);
	}

	@PreDestroy
	public void preDestroy() {
		channel.shutdown();
	}

	public boolean call(String id) {
		log.info("Calling dago-preprocessor with id {}", id);
		ProcessFileResponse response = null;

		try {
			response = stub.withDeadlineAfter(DEADLINE, TimeUnit.MINUTES)
						   .process(ProcessFileRequest.newBuilder()
													  .setId(id)
													  .build());
			if (Context.current()
					   .isCancelled()) {
				throw new StatusRuntimeException(Status.CANCELLED.withDescription("Cancelled by client"));
			}

			log.debug("Received: {}", response);
		} catch (StatusRuntimeException e) {
			log.warn("RPC failed: {}", e.getStatus());
		}

		return response != null && response.getResult();
	}
}
