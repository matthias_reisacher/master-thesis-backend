package at.tu.dago.core.psql.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = { "at.tu.dago.core.psql" })
public class PostgresConfig {

}
