package at.tu.dago.core.service;

import at.tu.dago.core.psql.dao.GraphDAO;
import at.tu.dago.core.psql.model.Graph;
import at.tu.dago.core.psql.model.Page;
import at.tu.dago.core.redis.model.GraphPage;
import at.tu.dago.core.storage.AttributeStorage;
import at.tu.dago.core.storage.GraphPageStorage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class GraphService {

	private GraphPageStorage storage;
	private AttributeStorage attributeStorage;
	private GraphDAO graphDAO;

	@Autowired
	public GraphService(GraphPageStorage storage, AttributeStorage attributeStorage, GraphDAO graphDAO) {
		this.storage = storage;
		this.attributeStorage = attributeStorage;
		this.graphDAO = graphDAO;
	}

	public Optional<GraphPage> getGraphPage(long graphId, int pageNumber, int depth) {
		//		Optional<GraphPage> graphPage = storage.getGraphPageOfDepthFromCache(graphId, pageNumber, depth);
		//
		//		if (!graphPage.isPresent()) {
		//			graphPage = storage.getGraphPageOfDepthFromDatabase(graphId, pageNumber, depth);
		//
		//			// Store object inside cache for future access
		//			graphPage.ifPresent(gp -> storage.cacheGraphPage(gp));
		//		}

		// Bypass caching due to major performance issues in redis
		return storage.getGraphPageOfDepthFromDatabase(graphId, pageNumber, depth);
	}

	public int countPagesOfGraph(long graphId) {
		return storage.countPagesOfGraph(graphId);
	}

	public int depthOfPage(long graphId, int pageNumber) {
		Optional<Integer> depth = storage.depthOfPage(graphId, pageNumber);
		return depth.orElse(-1);
	}

	public Long createGraph(String name, String description) {
		Graph newGraph = new Graph();
		newGraph.setName(name);
		newGraph.setDescription(description);
		newGraph.setTimestamp(new Date());

		newGraph = graphDAO.save(newGraph);

		return newGraph.getId();
	}

	public List<Graph> getGraphs() {
		return graphDAO.findAllByOrderByTimestampAsc();
	}

	public void clearLifetimeCacheOfLastPage(long graphId) {
		Optional<Page> page = storage.findLastPageOfGraph(graphId);
		page.ifPresent(p -> attributeStorage.clearLifetimesOfPage(p.getId()));
	}

	public void clearCache() {
		storage.clearCache();
	}
}
