package at.tu.dago.core.psql.dao;

import at.tu.dago.core.psql.model.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PageDAO extends JpaRepository<Page, Long> {

	@Query("SELECT p FROM Page p WHERE p.graph.id = :graph_id AND p.number = :number")
	Optional<Page> findByGraphAndNumber(@Param("graph_id") long graphId, @Param("number") int number);

	@Query("SELECT COUNT(p) FROM Page p WHERE p.graph.id = :graph_id")
	Integer countPagesOfGraph(@Param("graph_id") long graphId);

	@Query("SELECT MAX(p.number) FROM Page p WHERE p.graph.id = :graph_id")
	Integer maxPageNumberOfGraph(@Param("graph_id") long graphId);
}
