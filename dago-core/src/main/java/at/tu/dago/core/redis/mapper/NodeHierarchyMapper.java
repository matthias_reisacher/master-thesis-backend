package at.tu.dago.core.redis.mapper;

import at.tu.dago.core.psql.model.Node;
import at.tu.dago.core.redis.model.NodeHierarchy;
import org.mapstruct.Mapper;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
public interface NodeHierarchyMapper {

	default NodeHierarchy nodeHierarchy(long nodeId, List<Node> nodes) {
		NodeHierarchy nodeHierarchy = new NodeHierarchy();
		nodeHierarchy.setId(String.valueOf(nodeId));
		nodeHierarchy.setNodeIds(nodesToIds(nodes));

		return nodeHierarchy;
	}

	default List<Long> nodesToIds(List<Node> nodes) {
		List<Long> nodeIds = new ArrayList<>();
		nodes.forEach(node -> nodeIds.add(node.getId()));
		return nodeIds;
	}
}
