package at.tu.dago.core.redis.mapper;

import at.tu.dago.core.psql.model.Edge;
import at.tu.dago.core.psql.model.Node;
import at.tu.dago.core.redis.model.Lifetimes;
import org.mapstruct.Mapper;
import org.springframework.data.util.Pair;

import java.util.ArrayList;
import java.util.List;

import static at.tu.dago.core.redis.model.Lifetimes.ELEMENT_TYPE.Edges;
import static at.tu.dago.core.redis.model.Lifetimes.ELEMENT_TYPE.Nodes;

@Mapper(componentModel = "spring")
public interface LifetimesMapper {

	default Lifetimes nodeLifetimes(long pageId, List<Node> nodes) {
		Pair<List<Long>, List<Integer>> data = splitNodeData(nodes);
		return createLifetimes(Nodes, pageId, data.getFirst(), data.getSecond());
	}

	default Lifetimes edgeLifetimes(long pageId, List<Edge> nodes) {
		Pair<List<Long>, List<Integer>> data = splitEdgeData(nodes);
		return createLifetimes(Edges, pageId, data.getFirst(), data.getSecond());
	}

	default Pair<List<Long>, List<Integer>> splitNodeData(List<Node> nodes) {
		List<Long> ids = new ArrayList<>();
		List<Integer> values = new ArrayList<>();

		nodes.forEach(node -> {
			ids.add(node.getId());
			values.add(node.getAttribute()
						   .getLifetime());
		});

		return Pair.of(ids, values);
	}

	default Pair<List<Long>, List<Integer>> splitEdgeData(List<Edge> edges) {
		List<Long> ids = new ArrayList<>();
		List<Integer> values = new ArrayList<>();

		edges.forEach(edge -> {
			ids.add(edge.getId());
			values.add(edge.getAttribute()
						   .getLifetime());
		});

		return Pair.of(ids, values);
	}

	default Lifetimes createLifetimes(Lifetimes.ELEMENT_TYPE type, long pageId, List<Long> ids, List<Integer> values) {
		Lifetimes lifetimes = new Lifetimes();
		lifetimes.setType(type);
		lifetimes.setPageId(pageId);
		lifetimes.setIds(ids);
		lifetimes.setValues(values);

		return lifetimes;
	}
}
