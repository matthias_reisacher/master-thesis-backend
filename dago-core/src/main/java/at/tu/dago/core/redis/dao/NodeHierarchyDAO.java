package at.tu.dago.core.redis.dao;

import at.tu.dago.core.redis.model.NodeHierarchy;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NodeHierarchyDAO extends CrudRepository<NodeHierarchy, String> {}
