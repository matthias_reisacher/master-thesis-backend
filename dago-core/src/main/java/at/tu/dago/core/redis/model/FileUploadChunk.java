package at.tu.dago.core.redis.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;

@RedisHash("FileUploadChunk")
@Getter
@NoArgsConstructor
@Slf4j
public class FileUploadChunk implements Serializable {

	@Id
	private String id;

	@Setter
	private int number;

	@Setter
	private String content;

	@Setter
	private int size;

	/**
	 * Sets {@link FileUploadChunk#id} to the calculated redis hash.
	 */
	public void calcId(String id) {
		this.id = calcRedisHash(id, this.number);
	}

	/**
	 * Creates the hash code from {@link FileUploadChunk#id} and{@link FileUploadChunk#number}.
	 */
	public static String calcRedisHash(String id, int number) {
		if (id == null || id.isEmpty()) {
			log.warn("Hash code of FileUploadChunk is calculated with invalud values!");
		}

		return id + "-" + number;
	}

	@Override
	public String toString() {
		return String.format("FileUploadChunk(id=%s, number=%d, content=[sizeOf=%d], size=%d)", id, number, content.length(),
							 size);
	}
}
