package at.tu.dago.core.service;

import at.tu.dago.core.redis.dao.FileUploadChunkDAO;
import at.tu.dago.core.redis.dao.FileUploadMetadataDAO;
import at.tu.dago.core.redis.model.FileUploadChunk;
import at.tu.dago.core.redis.model.FileUploadMetadata;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

@Service
@Slf4j
public class FileUploadService {

	private FileUploadMetadataDAO metadataDAO;
	private FileUploadChunkDAO chunkDAO;
	private StringRedisTemplate jedis;

	@Autowired
	public FileUploadService(FileUploadMetadataDAO metadataDAO, FileUploadChunkDAO chunkDAO, StringRedisTemplate jedis) {
		this.metadataDAO = metadataDAO;
		this.chunkDAO = chunkDAO;
		this.jedis = jedis;
	}

	public void initUpload(String uploadId, long graphId, String name, String type, long size) {
		FileUploadMetadata metadata = new FileUploadMetadata();
		metadata.setId(uploadId);
		metadata.setGraphId(graphId);
		metadata.setName(name);
		metadata.setType(type);
		metadata.setSize(size);

		log.debug("Cached: {}", metadataDAO.save(metadata));
	}

	public void cacheChunk(String uploadId, int number, String content, int size) {
		FileUploadChunk chunk = new FileUploadChunk();
		chunk.setNumber(number);
		chunk.setContent(content);
		chunk.setSize(size);

		chunk.calcId(uploadId);

		log.debug("Cached: {}", chunkDAO.save(chunk));
	}

	public boolean finalizeUpload(String uploadId, int range) {
		// Fetch metadata
		Optional<FileUploadMetadata> metadata = metadataDAO.findById(uploadId);
		if (metadata.isPresent()) {
			log.debug("Fetched from Cache: {}", metadata.get());
		} else {
			log.warn("Could not fetch metadata for file upload with id {}", uploadId);
			return false;
		}

		// Fetch all chunks
		IntStream numbers = IntStream.rangeClosed(1, range);
		List<FileUploadChunk> chunks = chunkDAO.findByPageProperties(uploadId, numbers);
		log.debug("Fetched {} chunks from Cache", chunks.size());

		// Verify completion of chunks
		if (!verifyChunkCompletion(metadata.get(), chunks)) {
			log.warn("Could not verify file size - chunks of file upload with uploadId {} are not complete.", uploadId);
			return false;
		}

		// Unify chunks and cache the result
		String content = unifyChunks(chunks);
		log.debug("Uploaded file:\n{}", content);
		cacheFileContent(metadata.get(), content);

		cleanup(uploadId, chunks);

		return true;
	}

	private boolean verifyChunkCompletion(FileUploadMetadata metadata, Iterable<FileUploadChunk> chunks) {
		int chunkSize = 0;

		for (FileUploadChunk chunk : chunks) {
			chunkSize += chunk.getSize();
		}

		log.debug("Chunks have the size of {} - metadata specifies a size of {}", chunkSize, metadata.getSize());
		return chunkSize == metadata.getSize();
	}

	private String unifyChunks(List<FileUploadChunk> chunks) {
		chunks.sort(Comparator.comparing(FileUploadChunk::getNumber));
		StringBuilder builder = new StringBuilder();

		for (FileUploadChunk chunk : chunks) {
			builder.append(chunk.getContent());
		}

		return builder.toString();
	}

	private void cacheFileContent(FileUploadMetadata metadata, String content) {
		jedis.boundValueOps("file:" + metadata.getId() + ":metadata")
			 .set(metadata.toJSON());

		jedis.boundValueOps("file:" + metadata.getId() + ":content")
			 .set(content);

		log.debug("Cached file content for id {}", metadata.getId());
	}

	private void cleanup(String id, Iterable<FileUploadChunk> chunks) {
		log.debug("Freeing cache from file upload helpers");
		metadataDAO.deleteById(id);
		chunkDAO.deleteAll(chunks);
	}
}
