package at.tu.dago.core.utils;

import java.util.Collection;

public class Utils {

	public static int size(Iterable data) {
		if (data instanceof Collection) {
			return ((Collection<?>) data).size();
		}

		int counter = 0;
		for (Object ignored : data) {
			counter++;
		}

		return counter;
	}
}
