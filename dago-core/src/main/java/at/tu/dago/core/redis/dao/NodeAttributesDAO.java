package at.tu.dago.core.redis.dao;

import at.tu.dago.core.redis.model.NodeAttributes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NodeAttributesDAO extends CrudRepository<NodeAttributes, String> {}
