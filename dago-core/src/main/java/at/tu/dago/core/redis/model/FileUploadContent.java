package at.tu.dago.core.redis.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;

@RedisHash("FileUploadChunk")
@Setter
@Getter
@NoArgsConstructor
public class FileUploadContent implements Serializable {

	@Id
	private String id;
	private String content;

	@Override
	public String toString() {
		return String.format("FileUploadContent(id=%s, content=[sizeOf=%d])", id, content.length());
	}
}
