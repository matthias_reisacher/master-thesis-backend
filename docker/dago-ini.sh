#!/bin/bash
cd /home/dago/
if [[ -f compile-protos.sh ]]; then
    ./compile-protos.sh
    echo "Successfully compiled Protocol Buffers"
else
    (>&2 echo "Could not compile protos")
    exit 1
fi

[[ -f gradlew ]] && ./gradlew clean bootJar

if [[ ! -f /home/dago-run.sh ]]; then
    echo "#!/bin/bash" > /home/dago-run.sh
    echo "cd /home/dago/" >> /home/dago-run.sh
    echo "java -jar $(find dago-web/build/libs/ -type f -name 'dago-*.jar')" >> /home/dago-run.sh
    chmod +x /home/dago-run.sh
    echo "Successfully created dago-run.sh file"
fi

exec /home/dago-run.sh
