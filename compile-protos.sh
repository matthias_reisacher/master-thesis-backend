#!/usr/bin/env bash
set -o errexit
set -o nounset
#set -o xtrace

die() {
    echo "$@" >&2
    exit 1
}

proc_protos() {
    protoc -I=${VOLUME_PROTO} ${VOLUME_PROTO}/${1}/*.proto \
    --java_out=${SCR_DIR} \
    --plugin=protoc-gen-grpc-java=${GRPC_JAVA} \
    --grpc-java_out=${SCR_DIR}
}

GRPC_JAVA="${GRPC_JAVA_PLUGIN}/compiler/build/exe/java_plugin/protoc-gen-grpc-java"

[[ ! -d ${VOLUME_PROTO} ]] && die 'Error: Proto volume not found!'
[[ ! -f ${GRPC_JAVA} ]] && die 'Error: gRPC plugin not found!'

cd $(dirname "$0")
SCR_DIR='./dago-core/src/main/java'
TARGET_DIR="${SCR_DIR}/at/tu/dago/core/grpc"

[[ -d ${TARGET_DIR} ]] && rm -r ${TARGET_DIR}
mkdir -p ${TARGET_DIR}

proc_protos "utils"
proc_protos "dago-backend"
proc_protos "dago-backend/utils"
proc_protos "dago-preprocessor"
